<?php
class Page_m extends MY_Model
{
	protected $_table_name = 'pages';
	protected $_order_by = 'parent_id, order';
	public $rules = array(
		'parent_id' => array(
			'field' => 'parent_id', 
			'label' => 'Parent', 
			'rules' => 'trim|intval'
		), 
		'template' => array(
			'field' => 'template', 
			'label' => 'Template', 
			'rules' => 'trim|required|xss_clean'
		), 
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|max_length[100]|xss_clean'
		), 
		'slug' => array(
			'field' => 'slug', 
			'label' => 'Slug', 
			'rules' => 'trim|required|max_length[100]|url_title|callback__unique_slug|xss_clean'
		), 
		'body' => array(
			'field' => 'body', 
			'label' => 'Body', 
			'rules' => 'trim|required'
		),
		'morecontent' => array(
			'field' => 'morecontent', 
			'label' => 'More content', 
			'rules' => 'trim'
		)
	);

     //create a new empty object//Create your generic data object.
	public function get_new ()
	{
		$page = new stdClass();
		$page->title = '';
		$page->slug = '';
		$page->body = '';
		$page->morecontent = '';
		$page->img_source = '';
		$page->parent_id = 0;
		$page->template = 'page';
		return $page;
	}

    /*return page slug if it is set*/
	public function get_event_link(){
		$page = parent::get_by(array('template' => 'event'), TRUE);
		return isset($page->slug) ? $page->slug : '';
	}

     /*return page slug if it is set*/
	public function get_artist_link(){
		$page = parent::get_by(array('template' => 'artistspage'), TRUE);
		return isset($page->slug) ? $page->slug : '';
	}
	
	public function delete ($id)
	{
		// Delete a page
		parent::delete($id);
		
		// Reset parent ID for its children
		$this->db->set(array(
			'parent_id' => 0
		))->where('parent_id', $id)->update($this->_table_name);
	}

  /*function called by order_ajax method in page controller*/
	public function save_order ($pages)
	{
		if (count($pages)) {
			foreach ($pages as $order => $page) {
				if ($page['item_id'] != '') {
					$data = array('parent_id' => (int) $page['parent_id'], 'order' => $order);
					$this->db->set($data)->where($this->_primary_key, $page['item_id'])->update($this->_table_name);
				}
			}
		}
	}
 

/** [get page data from database and store into array. loop through the pages, and if the pages dont have any parents and are
included in the main_menu, add the pages to the array and return the array] */
	public function get_nested ()
	
	{
		$this->db->order_by($this->_order_by);
		$pages = $this->db->get('pages')->result_array();
		
		$array = array();
		foreach ($pages as $page) {
			if (! $page['parent_id'] && $page['main_menu'] ==1){
				// This page has no parent
				$array[$page['id']] = $page;
			}
			//else {
				// This is a child page
				//$array[$page['parent_id']]['children'][] = $page;
			//}
		}
		return $array;
	}

    /** [get all pages for administration page index.] */
	public function get_with_parent ($id = NULL, $single = FALSE)
	{
		$this->db->select('pages.*, p.slug as parent_slug, p.title as parent_title');
		$this->db->join('pages as p', 'pages.parent_id=p.id', 'left');
		return parent::get($id, $single);
	}

    /** [get pages with no parents.these have a value of 0 in the database. we need this method in the page_controller/edit function] */
	public function get_no_parents ()
	{
		// Fetch pages without parents
		$this->db->select('id, title');
		$this->db->where('parent_id', 0);
		$pages = parent::get();
		
		// Return key => value pair array
		$array = array(
			0 => 'No parent'
		);
		if (count($pages)) {
			foreach ($pages as $page) {
				$array[$page->id] = $page->title;
			}
		}
		
		return $array;
	}

   /*function to insert image into the database. Use the url segment to get the $id for 'where' arguement*/
  function insert_images($image_data = array()){
      $data = array(
          'img_source' => $image_data['file_name'],
         
      );
      
     $this ->db->where('id', $this->uri->segment(4));
     $this->db->update('pages', $data);

    
	}


}//end of class