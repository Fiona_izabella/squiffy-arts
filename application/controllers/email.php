<?php
class Email extends Frontend_Controller {

    public function __construct(){
        parent::__construct();
       
    }

    function index(){
        $this->data['subview'] = 'contact_form';
        $this->data['confirmation'] = " ";
        $this->load->view('_main_layout', $this->data);

    }

    function send(){

        $this->load->library('form_validation');
        //now we can access it as a class and use its methods

        //exact field name, error message(human name for field), validation rules
        $this->form_validation->set_rules('name', 'Name','trim|required');
        $this->form_validation->set_rules('email', 'Email','trim|required|valid_email');
        $this->form_validation->set_rules('message', 'Message','trim|required');

        //we need to run the validation
        if($this->form_validation->run() == FALSE){

             //reload if validation fails
            $this->data['subview'] = 'contact_form';
            $this->data['confirmation'] = " ";
            $this->load->view('_main_layout', $this->data);

            } else{
            //validation has passed and send email
            $name = $this->input->post('name');//same as $_POST['name'];
            $email = $this->input->post('email');
             $message = $this->input->post('message');

            //set the configuration for sending email
            $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user'=> 'fionka78@googlemail.com',
            'smtp_pass'=> 'webmistress78'
            );

            /*note: add  'mailtype' => 'html' into config array wityh </br> tages to have new lines in message*/
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($email, $name);
            $this->email->subject('contact form');
            $this->email->to('fionka78@gmail.com');
            $this->email->message("this was sent from $name with email of $email and the message is $message!");

              if($this->email->send()){

               // redirect to current page with flashdata message;
                $this->session->set_flashdata('message', 'Thankyou, your email has been sent');
                redirect(current_url());

                } else{

                show_error($this->email->print_debugger());
                }

    	
          }


}

}//end of class