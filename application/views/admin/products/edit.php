<div class = "row-fluid">
	<div class = "span9 offset1">
		<div id="top"></div>
		<h3><?php echo empty($products->id) ? 'Add a new product' : 'Edit product with id ' . $products->id; ?></h3>
		<?php echo form_open(); ?>
		<table class="table">

			<td>Name</td>
			<td><?php echo form_input('name', set_value('name', $products->name), 'title="Keep name short"'); ?><?php echo form_error('name', '<div class="errorArticle">', '</div>'); ?></td>
		</tr>
		<tr>
			<td>Price</td>
			<td><?php echo form_input('price', set_value('price', $products->price), 'title="price should be in numbers"'); ?><?php echo form_error('price', '<div class="errorArticle">', '</div>'); ?></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><?php echo form_textarea('description', set_value('description', $products->description), 'class="ckeditor" , id="textarea_id" '); ?><?php echo form_error('description', '<div class="errorArticle">', '</div>'); ?></td>
		</tr>
		<tr>
			<!--create a dropdown selection for category'-->
			<tr>
				<td>Category</td>
				<td>
					<select name="category">
						<option value="art" <?php echo set_select('category', 'art'); ?> >art</option>
						<option value="poster" <?php echo set_select('category', 'poster'); ?> >poster</option>
						<option value="souvenir" <?php echo set_select('category', 'souvenir'); ?> >souvenir</option>
					</select> 
				</td>
			</tr>
			<td></td>
			<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>

		</tr>
		<tr>
			<td></td>
			<td></td>
		</tr>


	</table>

	<?php echo form_close();?>



	<!--check to see if event has already been created, if yes then whether it has an image and print to page accordingly-->
<?php if (!isset($products->id)):?>
<?php $products->id = NULL;
$products->image = NULL;
$currentImage = "You have no image uploaded";?>
<p><?php echo $currentImage;?></p>

<?php elseif (isset($products->id) && !isset($products->image)): 
$currentImage = "You have no image uploaded yet";?>
<p><?php echo $currentImage;?></p>

<?php else: 
$currentImage = "Your current image";?>
<p><?php echo $currentImage;?></p>
<?php $img = $products->image; ?>
<div class="row-fluid">
<div class="span4 offset2 currentImage"><?php echo img('assets/images/products/'.$img); ?></div>
</div>
<?php endif; ?>




	<br /><br />
	<p><?php //echo anchor('admin/products/do_upload/' . $products->id, 'upload different image','class="btn-custom btn-empty"' ); ?></p>
	<br /><br />
	<p><?php echo anchor('admin/products','back to products', 'class="btn btn-primary btn-custom"'); ?></p>
	<br />
	<p><a href="<?=current_url();?>#top" class="btn btn-primary btn-custom">Go to top of page</a></p>



</div>
</div>
