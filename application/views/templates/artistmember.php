<!--content-->
<div class="row-fluid">
  <section class="span12">

    <!--artist menu at top of section-->
    <div class="row-fluid addMenuStyle">
      <?php if(count($artists)): foreach($artists as $artist): ?>
        <div class="span1 addMenuStyleLink">  
          <?php $url = artist_link($artist); ?>
          <h3><?php echo anchor($url, e($artist->fname)); ?></h3>
        </div>
      <?php endforeach; ?>
    <?php else: ?>
      <td colspan="3">We could not find any artists.</td>
    <?php endif; ?> 
  </div>
  <!--end of artists menu-->


  <div class="row-fluid">
    <article class="span6 artist">
      <h2><?php echo e($artistMain->fname); ?></h2>
      <p class="date"><?php echo e($artistMain->fname). ' ' .e($artistMain->lname); ?></p>
      <p class="date"><?php echo e($artistMain->email); ?></p>
      <div class="artistImages">
      <!--display the images-->
        <?php $img = e($artistMain->img_source1); ?>
        <?php $img2 = e($artistMain->img_source2); ?>
        <?php $img3 = e($artistMain->img_source3); ?>
        <?php echo img('uploads/thumbs/'.$img); ?>
        <?php echo img('uploads/thumbs/'.$img2); ?>
        <?php echo img('uploads/thumbs/'.$img3); ?>
        <div class="clearfix"></div>
      </div>

    </article>



    <article class="span6">
      <h3> profile</h3>
      <?php echo $artistMain->profile; ?> 

      <div class="row-fluid">
        <div class="span12">
          <!--store link values into a variable to use for anchor links below-->
          <?php $outsideUrl = $artistMain->facebook; ?>
          <?php $twitterUrl = $artistMain->twitter; ?>
          <?php $blogUrl = $artistMain->blog; ?>
          <?php $websiteUrl = $artistMain->website; ?>

          <!--linksnav-->
          <div class="row-fluid">
            <div class="span3"><h8 class="wider2"><?php echo anchor_popup($outsideUrl, 'facebook'); ?></h8></div>
            <div class="span3"><h8 class="wider2"><?php echo anchor_popup($websiteUrl, 'website'); ?></h8></div>
            <div class="span3"><h8 class="wider2"><?php echo anchor_popup($twitterUrl, 'twitter'); ?></h8></div>
            <div class="span3"><h8 class="wider5"><?php echo anchor_popup($blogUrl, 'blog'); ?></h8></div>

          </div> 
          <!--end of linksnav-->


        </div>
      </div>


    </article>

  </div> 
</section>
</div>
<!--end of content-->