<?php
class Reviews extends Admin_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('support_m');
	}

	public function index ()
	{
		// Fetch all reviews
		$this->data['reviews'] = $this->support_m->get();
		
		// Load view
		$this->data['subview'] = 'admin/reviews/index';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function edit ($id = NULL)
    {
        // Fetch a review or set a new one
        //need this for the set value() in the view
        if ($id) {
            $this->data['reviews'] = $this->support_m->get($id);
            count($this->data['reviews']) || $this->data['errors'][] = 'article could not be found';
        }
        else {
            $this->data['reviews'] = $this->support_m->get_new();
        }
        
        // Set up the form
        // firstset up the rules made in support_m
        $rules = $this->support_m->rules;
        $this->form_validation->set_rules($rules);
        
        // Process the form
        //iif passes validation pass the data from the input fields into the $data array which is saved to the database  
        //this function is found in MY_MODEL
        if ($this->form_validation->run() == TRUE) {
            $data = $this->support_m->array_from_post(array(
                'title', 
                'name', 
                'body', 
                'create'
            ));
            $this->support_m->save($data, $id);
            redirect('admin/reviews');
        }
		
		// Load the view
		$this->data['subview'] = 'admin/reviews/edit';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function delete ($id)
	{
		$this->support_m->delete($id);
		redirect('admin/reviews');
	}

}