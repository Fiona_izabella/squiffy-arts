<?php $this->load->view('components/page_head');?>
	   <nav class="navbar">
	 	<div class="navbar-inner">
	 		<div class="container">
              <?php echo get_menu($menu); ?>
	 		</div>
	 	</div>
	 </div>
 </nav>
 </div><!--end of header class row-->
 </header>
 <!--end of header-->

 	 <!--content-->
 	     <div class="row-fluid">
           <div class="span12">

              <section class="row-fluid">            
                <div class="span12">
			     <?php $this->view($contents); ?>
			      <div>
			  </section>		
           
			</div>
 		</div>
<?php $this->load->view('components/page_tail');?>
		
		
			
	