<?php echo '<h1 class="products">'.$heading.'</h1>'; ?>

<ul class="shop">
<!-- //Because the product data is being returned in a array, we have to use foreach in order to display all products -->

		<?php foreach($products as $p): ?>
  		<li>  
          <h3><?php echo $p['name']; ?></h3>  
          <!--remove '.jpg' extension from image source for adding to alt attribute-->
          <?php $withoutExt = preg_replace("/\\.[^.\\s]{3,4}$/", "", $p['image']); ?>
          <img src="<?php echo base_url(); ?>assets/images/products/<?php echo $p['image']; ?>" alt="image of product: <?php echo $withoutExt  ;?>" />  
          <p class="highlight">&pound;<?php echo $p['price']; ?></p> 
           <p><?php echo $p['description']; ?></p> 

          <?php echo form_open('shop/add'); ?>  
              <fieldset>  
                  <label>Quantity</label>  
                  <?php echo form_input('quantity', '1', 'maxlength="2"'); ?>  
                  <?php echo form_hidden('id', $p['id']); ?>
                  <?php echo form_submit('add', 'add'); ?>
              </fieldset>  
          <?php echo form_close(); ?>  
      </li>  
     
		<?php endforeach;?>
</ul>
	
