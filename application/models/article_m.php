<?php
class Article_m extends MY_Model
{
	protected $_table_name = 'articles';
	protected $_order_by = 'created desc, id desc';
	protected $_timestamps = TRUE;
	//create the rules for validation
	//these rules will be called in the article controller for validation
	public $rules = array( 
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|max_length[100]|xss_clean'
		), 
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		), 
		'body' => array(
			'field' => 'body', 
			'label' => 'Body', 
			'rules' => 'trim|required|max_length[655]|'
		)
	);

    //create a new empty object//Create your generic data object.
    //this could also be done using an Associative array.
  
	public function get_new ()
	{
		$article = new stdClass();
		$article->title = '';
		$article->name = '';
		$article->body = '';
		return $article;
	}
	

}