<div class = "row-fluid">
	<div class = "span10 offset1">
		<section>
			<h2>Products</h2>
			<?php echo anchor('admin/products/edit', '<i class="icon-plus"></i> Add a product','class="addArticle"'); ?>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
						<th>Price</th>
						<th>category</th>
						<th>Edit</th>
						<th>Add/Edit image</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tbody>
					<?php if(count($products)): foreach($products as $product): ?>	
						<tr>
							<td><?php echo anchor('admin/products/edit/' . $product->id, $product->name); ?></td>
							<td><?php echo $product->price; ?></td>
							<td><?php echo $product->category; ?></td>
							<td><?php echo btn_edit('admin/products/edit/' . $product->id); ?></td>
							<td><?php echo btn_edit('admin/products/do_upload/' . $product->id); ?></td>
							<td><?php echo btn_delete('admin/products/delete/' . $product->id); ?></td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="3">We could not find any products.</td>
					</tr>
				<?php endif; ?>	
			</tbody>
		</table>
	</section>

</div>
</div>