<?php
class User_M extends MY_Model
{
	/** @var string [create the table name called users] */
	protected $_table_name = 'users';
	/** @var string [variable which decides how to order the data] */
	protected $_order_by = 'name';
	/** @var array [create the rules and save in array] */
	public $rules = array(
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|xss_clean'
		), 
		'password' => array(
			'field' => 'password', 
			'label' => 'Password', 
			'rules' => 'trim|required'
		)
	);
	/** @var array [create the ADMIN rules and save in array] */
	public $rules_admin = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|xss_clean'
		), 
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			//add the 'unique_mail'rule by using a callback
			'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'
		), 
		'role' => array(
			'field' => 'role', 
			'label' => 'role', 
			'rules' => 'trim|required'
		), 
		'password' => array(
			'field' => 'password', 
			'label' => 'Password', 
			'rules' => 'trim|matches[password_confirm]'
		),
		'password_confirm' => array(
			'field' => 'password_confirm', 
			'label' => 'Confirm password', 
			'rules' => 'trim|matches[password]'
		),
	);

	function __construct ()
	{
		parent::__construct();
	}

    
   /**
    * [method for logging in.
    *get email, role and password from database.
    *if user with above credientials exists, set the session with the $data array]
    * @return [set the session and return a boolean] [description]
    */
	public function login ()
	{
		$user = $this->get_by(array(
			'email' => $this->input->post('email'),
			'role' => $this->input->post('role'),
			'password' => $this->hash($this->input->post('password')),
		), TRUE);
		
		if (count($user)) {
			//if a user with the above credientials is found.
			// Log in user
			$data = array(
				'name' => $user->name,
				'email' => $user->email,
				'id' => $user->id,
				'role' => $user->role,
				'loggedin' => TRUE,
			);
			//add the data to the session array
			//return true if it works otherwise return false
			$this->session->set_userdata($data);
			return true;

		   } else {

		   return false;
	   }
	}

    /** [log out user by destroying the session] */
	public function logout ()
	{
		$this->session->sess_destroy();
	}

    /*method to determine whether user is logged in or not*/
    /** [returns boolean value] */
	public function loggedin ()
	{
		return (bool) $this->session->userdata('loggedin');
	}

	// Check if username exists
	// this function works with ajax call
	public function username_exists($name)
	{
		$this->db->where('name', $name);
		$this->db->from('users');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return TRUE;
			/*print_r($query);*/
		} else {
			return FALSE;
		}
	}


// Check if username matches the username of a certain id
// // this function works with ajax call
	public function username_existsWithId($name, $id)
	{

		$array = array('name' => $name, 'id' => $id);
        $this->db->where($array); 
        $this->db->from('users');
        $query = $this->db->get();

		if ($query->num_rows() > 0) {
			return FALSE;//return false if the user with the id HAS that name
		} else {
			return TRUE;//return true if user with the id does NOT have that name
		}
	}

    
// Check if email exists
// this function works with ajax call
	public function email_exists($email)
	{
		$this->db->where('email', $email);
		$this->db->from('users');
		$query = $this->db->get();

		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

    /** [funciton to check is user role is 'admin' which is the main administration] */
	public function is_administration ()
	{
		//returns a true of false if user is administration or not
		//need a if session userdata == admin
		//return (bool) $this->session->userdata('is_admin');
		   if ($this->session->userdata('role') == 'admin'){

		   	return TRUE;
		   } else {
		   	return FALSE;
		   }

	}
	//so that we always have a valid user object and dont get errors if there no users
	public function get_new(){
		$user = new stdClass();
		$user->name = '';
		$user->email = '';
		$user->role = '';
		$user->password = '';
		return $user;
	}
  
  /*method used with function login() above to hash password for security*/
	public function hash ($string)
	{
		return hash('sha512', $string . config_item('encryption_key'));
	}
}