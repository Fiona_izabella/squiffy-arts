<div class="row-fluid">
  <div class="span2 offset3"><h6 class="wider"><?php echo anchor('about','about'); ?></h6></div>
  <div class="span2"><h6 class="wider5"><?php echo anchor('pressrelease','press'); ?></h6></div>
  <div class="span2"><h6 class="wider2"><?php echo anchor('reviews','reviews'); ?></h6></div>

</div> 
<!--end of subnav-->

<!--pagination-->
<div class="row-fluid">
  <div class="span12">
  <!--if there are enough reviews to result in pagination, then dsiplay the pagination-->
    <?php if($pagination): ?>
      <section class="pagination"><?php echo $pagination; ?></section>
    <?php endif; ?>
  </div>
</div>
<!--end of pagination-->

<!--content-->
<div class="row-fluid">
  <section class="span12">

    <div class="row-fluid">
      <article class="span6">
        <h2><?php echo $title; ?></h2>
        <h3>Total number of reviews..<?php echo $reviewcount;?></h3>

        <section>
          <table class="table reviews">
            <tbody>
              <?php if(count($reviews)): foreach($reviews as $review): ?>  
<!--this comes from the array $reviews in the controller that comes from the get() function
  $this->data['reviews'] = $this->support_m->get();-->
  <tr><td><span class="titles">Name of Reviewer: </span><?php echo $review->name; ?></td></tr>
  <tr><td><span class="titles">Title: </span><?php echo $review->title; ?></td></tr>
  <tr><td><span class="titles">Created: </span><?php echo date('g:i a', strtotime($review->create)) . ' on '. date('d/m/Y', strtotime($review->create)); ?></td></tr>

  <tr class="endOfSect"><td><span class="titles">Review: <br></span><p><?php echo $review->body; ?></p></td> </tr>


<?php endforeach; ?>

<?php else: ?>
  <tr>
    <td colspan="3">We could not find any articles.</td>
  </tr>
<?php endif; ?> 

</tbody>
</table>
</section>

</article>
<article class="span6">
  <h3>Add a review</h3>
  <!--Reviews FORM-->
  <?php echo form_open('reviews/addReview'); ?>
  <table class="table" id="addReview">
    <tr>
      <td>name</td>
      <td><?php echo form_input('name'); ?><?php echo form_error('name', '<div class="errorReview">', '</div>'); ?></td>
    </tr>
    <tr>
      <td>title</td>

      <td><?php echo form_input('title'); ?><?php echo form_error('title', '<div class="errorReview">', '</div>'); ?></td>
    </tr>
    <tr>
      <td>body</td>
      <td><?php echo form_textarea('body'); ?><?php echo form_error('body', '<div class="errorReview">', '</div>'); ?></td>
    </tr>
    <tr>
      <td></td>
      <td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>
    </tr>
  </table>
  <?php echo form_close();?>
</article>
</div> 
</section>
</div>
<!--end of content-->