<?php
class Shop_m extends MY_Model
{


	protected $_table_name = 'sales';
	protected $_order_by = 'created desc';
	protected $_timestamps = TRUE;
	public $rules = array(
		'fname' => array(
			'field' => 'fname', 
			'label' => 'fname', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
			), 
		'lname' => array(
			'field' => 'lname', 
			'label' => 'lname', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
			),
		'email' => array(
			'field' => 'email', 
			'label' => 'email', 
			'rules' => 'trim|required|valid_email|xss_clean'
			),
		'address' => array(
			'field' => 'address', 
			'label' => 'address', 
			'rules' => 'trim|required|max_length[100]|xss_clean'
			),
		'phonenumber' => array(
			'field' => 'phonenumber', 
			'label' => 'phonenumber', 
			'rules' => 'trim|required|max_length[15]|xss_clean'
			)
		);

	/*retreive products by their category. Used to organise which category is displayed in view*/
	function retreive_products($category){

		$this->db->where('category', $category); 
		$results = $this->db->get('products');

        return $results->result_array();//return as an array

    }

    /*retreive products by their $id. This function is used with the cart session*/
    function get($id) {

    	$results = $this->db->get_where('products', array('id' => $id))->result();
    	$result = $results[0];

    	return $result;
    }

     public function retreive_sales(){

		$this->db->order_by("created", "desc");
		$query = $this->db->get('sales');
		//order_by shoud be before get() methos
        //note: return $results->result();//return as an array
         return $query->result();//returns object

    }

     /** [save the sales data into database] */
     public function saveSales($data, $order_id = NULL){
		
		
		// Set timestamps $data['created'] field to $now value
		$this->_timestamps == TRUE; 
		$now = date('Y-m-d H:i:s');
		$order_id || $data['created'] = $now;


		!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
		$this->db->set($data);
		$this->db->insert($this->_table_name);
		$order_id = $this->db->insert_id();
				
		return $order_id;
		//need to return this for the saveOrderDetails() function
	}


   /** [save order details into database
        Pass in the $cart session and the order_id from the above saveSales()) function] */
	public function saveOrderDetails($cart, $orderIdsuccess){
		
		foreach ($cart as $item) {

			$data = array(
				'order_id' => $orderIdsuccess,
				'product_id' => $item['id'] 

				);

			$this->db->insert('orderDetails', $data); 
			// Produces: INSERT INTO orderDetails (order_id, product_id) VALUES ($orderIdsuccess, $item['id']);
			//this needs to be within the loop

		}

		return $orderIdsuccess;

	}


     /**join 2 tables to get more info about a particular order*/
    public function orderMoreinfo($order_id){

			$this->db->select('orderDetails.order_id, orderDetails.product_id, products.name, products.price');
			$this->db->from('orderDetails');
			$this->db->join('products', 'orderDetails.product_id = products.id');
			$this->db->where('order_id', $order_id); 
			$query = $this->db->get();

			return $query->result_array();
	}




   public function get_by($order_id){
		$this->db->where('order_id', $order_id); 
		$results = $this->db->get('sales');
		return $results->result();//returns as object  //$results->result_array();//returns as array
	
	}


    /*delete by $order_id*/
	public function deleteSale($order_id){
    //DELETE FROM sales WHERE order_id = $order_id
    $this->db->delete('sales', array('order_id' => $order_id)); 

	}
   


}//end of class
