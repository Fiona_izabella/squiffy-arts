<?php
class Watchlist extends Frontend_Controller {

    public function __construct(){
        session_start();
        parent::__construct();
        $this->load->model('event_m');
       
    }

    function add(){
        $eventId = $this->uri->segment(3);
        //note that we filter the array to not show any duplicates in the view
        //add the $eventId onto the session $watchlist array
        $_SESSION['watchlist'][] = $eventId;
        //pass the session into the array $watchlist 
        $data['watchlist'] = $_SESSION['watchlist'];
    	$this->data['view'] = 'watchlist/addwatchlist';
        $this->load->view('watchlist/index', $this->data);


    }

    function show(){
  	
     //$watchlist variable gets passed the session value or an empty array.
     //the $watchlist gets filtered in the view for duplicate events
     $this->data['watchlist'] = isset($_SESSION['watchlist']) ? $_SESSION['watchlist'] : array();

     if(empty($_SESSION['watchlist'])){
        

     $this->data['bg'] = TRUE;
     $this->data['message'] = "Your watchlist is empty";
     $this->data['view'] = 'watchlist/watchlist';
     $this->load->view('watchlist/index', $this->data);

        } else{

     $this->data['bg'] = FALSE;
     $this->data['message'] = "Your watchlist";
     $this->data['view'] = 'watchlist/watchlist';
     $this->load->view('watchlist/index', $this->data);

        }
   		
    

    }

     function delete(){
     	//remove from session
     	$Eventid= $this->uri->segment(3);

        foreach($_SESSION['watchlist'] as $k => $v) {
        if($v == $Eventid)
        unset($_SESSION['watchlist'][$k]);
         }

         redirect('watchlist/show');

     }


 }//end of class