<?php
class Feeds_m extends MY_Model{ 
	/*model which works with rss controller*/

	protected $_table_name = 'feeds';
	protected $_order_by = 'date desc, id desc';
	protected $_timestamps = TRUE;
	public $rules = array(
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		), 
		'text' => array(
			'field' => 'text', 
			'label' => 'text', 
			'rules' => 'trim|required'
		)
	);


      
    // get all rss feeds 
    function getFeeds($limit = NULL)  
    //set to null so that you dont have to set a limit
    {  
        return $this->db->get('feeds', $limit); 
        //select all from feeds. $limit is the limit parameter (optional); otherwise it returms
        //all records
    }  

    //save rss feeds
    public function savefeeds(){
    
		// Set timestamps
			$now = date('Y-m-d H:i:s');
			$data['date'] = $now;
			$data['title'] = $this->input->post('title');
			$data['text'] = $this->input->post('text');
		
	
		// Insert into database
			$this->db->insert($this->_table_name, $data);
			

	}

   



}//end of class  