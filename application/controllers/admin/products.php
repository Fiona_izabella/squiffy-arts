<?php
class Products extends Admin_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('products_m');
	}

	public function index ()
	{
		// Fetch all products
		$this->data['products'] = $this->products_m->get();
		
		// Load view
		$this->data['subview'] = 'admin/products/index';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function edit ($id = NULL)
    {
        
        // Fetch a product or set a new one
        //need this for the set value() in the view
        if ($id) {
            $this->data['products'] = $this->products_m->get($id);
            count($this->data['products']) || $this->data['errors'][] = 'product could not be found';
        }
        else {
            $this->data['products'] = $this->products_m->get_new();
        }
        
        // Set up the form
        // first set up the rules made in products_m
        $rules = $this->products_m->rules;
        $this->form_validation->set_rules($rules);
        
        // Process the form
        //if passes validation pass the data from the fields into the $data array which is saved to the database 
        //array_from_post() and save() functions are found in MY_MODEL
        if ($this->form_validation->run() == TRUE) {
            $data = $this->products_m->array_from_post(array(
                'name', 
                'price', 
                'description', 
                'category'
            ));
            $this->products_m->save_product($data, $id);
            redirect('admin/products');
        }
		
		// Load the view
		$this->data['subview'] = 'admin/products/edit';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function delete ($id)
	{
		$this->products_m->delete($id);
		redirect('admin/products');
	}

   /*upload product image*/
    function do_upload($id = NULL)
    {    

        if ($id) {
            $this->data['products'] = $this->products_m->get($id);
            count($this->data['products']) || $this->data['errors'][] = 'pruduct could not be found';
        }
        else {
            $this->data['products'] = $this->products_m->get_new();
        }

        $config['upload_path'] = './assets/images/products/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
    
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata($error);
            $page_id= $this->uri->segment(4);
            //// Load the view
        $this->data['subview'] = 'admin/products/editImage';
        $this->load->view('admin/_layout_main', $this->data);
        }
        else
        {   

            $this->upload->do_upload();
            $image_data = $this->upload->data();
            $config['image_library'] = 'gd2';
             $config['source_image'] = $image_data['full_path'];//this is the source
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 300;
            $config['height'] = 250;

            $this->load->library('image_lib', $config); 
            $this->image_lib->resize();
            $this->products_m->insert_images($this->upload->data());
            $data = array('upload_data' => $this->upload->data());
            $this->session->set_flashdata('message', $data);
            $page_id= $this->uri->segment(4);

        redirect('admin/products/do_upload/' . $page_id);

        }
    
    }


}