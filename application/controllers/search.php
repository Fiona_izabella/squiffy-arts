<?php
class Search extends Frontend_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('event_m');
    }

    public function index() {

               //store results from keyword_search() method in $query array
              $query = $this->event_m->keyword_search();
              //dump($query);

              if($query){
              //set error messages
              $this->data['errorMessage'] = "your search results....";
              //pass $query results array into $events array and load the page
              $this->data['events'] = $query;
              $this->data['subview'] = 'searchresult';
              $this->load->view('_main_layout', $this->data);
                   
              }
          
             else {
               $this->data['events'] = "";
               $this->data['errorMessage'] = "Sorry no results found";
               $this->data['subview'] = 'searchresult';
               $this->load->view('_main_layout', $this->data);

         
              }
                       
            
    }//end of index method
    

}//end of class