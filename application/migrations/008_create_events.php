<?php
class Migration_Create_events extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'date' => array(
				'type' => 'DATETIME',
			),
			'body' => array(
				'type' => 'TEXT',
			),
			'keywords' => array(
				'type' => 'VARCHAR',
				'constraint' => '255',
			),
			'img_source' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
			),
			'created' => array(
				'type' => 'DATETIME',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('events');
	}

	public function down()
	{
		$this->dbforge->drop_table('events');
	}
}