<?php $this->load->view('member/components/page_head'); ?>
    <div class="navbar navbar-static-top navbar-inverse">
	    <div class="navbar-inner">
		    <a class="brand" href="<?php echo site_url('member/dashboard'); ?>"><?php echo $member_title; ?></a>
		    <ul class="nav">
                 <!--use slug value to determine the 'active' class in the admin main menu-->
		    <?php $slug = $this->uri->segment(2);?>

			<li<?php if ($slug=="dashboard") 
			echo " class=\"active\""; ?>><a href="<?php echo site_url('member/dashboard'); ?>">Dashboard</a></li>
			<li<?php if ($slug=="event") 
			echo " class=\"active\""; ?>><?php echo anchor('member/event', 'events'); ?></li>		    			     
			<li<?php if ($slug=="artist") 
			echo " class=\"active\""; ?>><?php echo anchor('member/artist', 'artists'); ?></li> 
		     <li class="logout"><?php echo anchor('admin/user/logout', 'logout'); ?></li>
		     <li class="logout"><?php echo anchor('welcomepage', 'home'); ?></li>
		    </ul>
	    </div>
    </div>

	<!-- <div class="container"> -->
		<div class="row">
			<!-- Main column -->
			<div class="span12">
<?php $this->load->view($subview); ?>
			</div>			
		</div>
	<!-- </div> -->

<?php $this->load->view('admin/components/page_tail'); ?>