<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
 <meta name="keywords" content="squiffy arts festival " />
  <meta name="description" content="arts community festival in London" />
  <meta name="author" content="fiona przybylski" />
  <meta name="copyright" content="&copy;fiography" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
	<title>Galleries</title>
	<!-- Bootstrap and jquery css -->
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap.css" />
	<link rel="stylesheet" type="text/css" href="../assets/css/bootstrap-responsive.min.css" />
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css" />
     <!-- fancyBox -->
	<!-- Add jQuery library -->
	<script type="text/javascript" src="assets/fancybox/lib/jquery-1.10.1.min.js"></script>
	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="assets/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="assets/fancybox/source/jquery.fancybox.js?v=2.1.5"></script> 
	<link rel="stylesheet" type="text/css" href="assets/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="assets/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="assets/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="assets/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="assets/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
    <!--script to initialize fancybox-->
	<script type="text/javascript">
		$(document).ready(function() {

			$('.fancybox').fancybox();

			$(".fancybox-button").fancybox({
		prevEffect		: 'none',
		nextEffect		: 'none',
		closeBtn		: false,
		helpers		: {
			title	: { type : 'inside' },
			buttons	: {}
		}
	});		

});
	</script>
	
</head>
<body>
<div class="container">
        <div class="row-fluid">
            <div class="span12 hiddengallery">
	
	
	<h1>Squiffy Arts image gallery</h1>
	<p>     
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/fiona1.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/fiona1s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/fiona3.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/fiona3s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/fiona4.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/fiona4s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/fiona5.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/fiona5s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/1.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/1s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/2.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/2s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/3.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/3s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/4.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/4s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/5.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/5s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/6.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/6s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/7.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/7s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/8.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/8s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/9.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/9s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/14.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/14s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/17.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/17s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/20.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/20s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/21.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/21s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/22.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/22s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/23.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/23s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/24.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/24s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/25.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/25s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/26.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/26s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/27.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/27s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/11.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/11s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/28.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/28s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/mia5.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/mia5s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/mia6.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/mia6s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/mia4.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/mia4s.png" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/mia3.png" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/mia3s.png" alt="gallery image popup" /></a>	
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/untitledseries1.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/untitledseries1s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/untitledseries4.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/untitledseries4s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/untitledseries8.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/untitledseries8s.jpg" alt="gallery image popup" /></a>
		<a class="fancybox-button" rel="fancybox-button" href="assets/images/untitledseries9.jpg" data-fancybox-group="gallery" title="gallery image"><img src="assets/images/untitledseries9s.jpg" alt="gallery image popup" /></a>
	</p>
	</div>
	<h1><a href="welcome">exit gallery</a></h1>
	</div>
	</div>
</body>
</html>