<?php
class Subscription_m extends MY_Model
{
	protected $_table_name = 'subscriptions';
	protected $_order_by = 'create asc';
	protected $_timestamps = TRUE;
	//set the rules into an array and use this to run the validation
	public $rules = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		), 
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|xss_clean'
		), 
	);

    /*get all subscription from the subscriptions table*/
	public function get($id = NULL){

        $this->db->order_by('created', 'desc');
		$query = $this->db->get('subscriptions');
		

		if ($query->num_rows() > 0)
        {

        return $query->result();
        //returns data as an array of objects
        }

}

    /*get all subscription from the subscriptions table by their $id in order to edit them*/
     public function getby($id = NULL){
            $this->db->where('id', $id); 

             $query = $this->db->get_where('subscriptions', array('id' => $id));
             if ($query->num_rows() > 0) {

             
                foreach ($query->result_array() as $row)
                {
                 return $query->row();
			     //returns a single result row
                 }

             }

		

		}
		

    //create a new object, this will be used in the setvalue 
	public function get_new ()
	{
		$subscription = new stdClass();//create a new object, this will be used in the setvalue 
		$subscription->name= '';
		$subscription->email = '';
		$subscription->created = date('Y-m-d');
		return $subscription;
	}
	
	

}