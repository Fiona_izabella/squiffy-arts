<?php 

class Migration_Create_Sales extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'order_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'fname' => array(
				'type' => 'VARCHAR',
				'constraint' => 11
			),
			'lname' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),

			'address' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),

			'phonenumber' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			
		));
        
        $this->dbforge->add_key('order_id', TRUE);
		$this->dbforge->create_table('sales');
	}

	public function down()
	{
		$this->dbforge->drop_table('sales');
	}


}
