<!--subnav-->
   <div class="row-fluid">
        <div class="span2 offset3"><h7 class=""><?php echo anchor('surroundingarea', 'area'); ?></h7></div>
        <div class="span2"><h7 class="wider"><?php echo anchor('map', 'map'); ?></h7></div>
        <div class="span2"><h7 class="contact"><?php echo anchor('email', 'email us'); ?></h7></div>   
  </div> 
<!--end of subnav-->

<!--content-->
<div class="row-fluid">
  <section class="span8 offset2 contactBg">
          
              <article class="row-fluid"> 
              <div class="span12">
              <h2><?php echo e($page->title); ?></h2>
                 <div class="contactText">
                 <?php echo $page->body; ?> 
                 <?php echo $page->morecontent; ?>
                 </div> 
              </div>
              </article>                
               
    </section>
</div>
  <!--end of content-->