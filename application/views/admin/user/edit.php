<div class = "row-fluid">
<div class = "span10 offset1">

<h3><?php echo empty($user->id) ? 'Add a new user' : 'Edit user ' . $user->name; ?></h3>
<p class="inline">user id: </p><p id="userIdreference"><?php echo empty($user->id) ? '' : $user->id; ?></p>


<?php //echo form_open('user/register', array('id' => 'registration_form'));?>
<?php echo form_open('', array('id' => 'registration_form')); ?>
<table class="table user_form">
	<tr>
		<td class="title">Name</td>
		<?php $data = array(
              'name'        => 'name',
              'id'          => 'name',
              'maxlength'   => '100',
              'title'       => 'required',
            ); ?>
		<td><?php echo form_input($data, set_value('name', $user->name));?><?php echo form_error('name', '<div class="errorArticle">', '</div>'); ?>
		<p id="bad_name"></p></td>		
	</tr>
	<tr>
		<td class="title">Email</td>
		<?php $data = array(
              'name'        => 'email',
              'id'          => 'email',
              'maxlength'   => '100',
              'title'       => 'required',
            ); ?>
		<td><?php echo form_input($data, set_value('email', $user->email)); ?><?php echo form_error('email', '<div class="errorArticle">', '</div>'); ?>
		<p id="bad_email"></p></td>
	</tr>
	<tr>
		<td>Password</td>
		<td><?php echo form_password('password'); ?><?php echo form_error('password', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>Confirm password</td>
		<td><?php echo form_password('password_confirm'); ?><?php echo form_error('password_confirm', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
<!--create a dropdown selection for value of 'role'-->
	<tr>
	<td>Role</td>
	<td>
	<select name="role">
	<option value="admin" <?php echo set_select('roles', 'admin'); ?> >admin</option>
	<option value="artist" <?php echo set_select('roles', 'artist'); ?> >artist</option>

	</select> 
	</td>
	</tr>

	<td></td>
	<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>
	</tr>
</table>
<?php echo form_close();?>


</div>
</div>

