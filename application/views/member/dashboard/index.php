<div class = "row-fluid">
	<div class = "span9 offset1">
		<h2>Recently modified events</h2>

		<?php if(count($recent_events)): ?>
			<ul>
				<?php foreach($recent_events as $event): ?>
					<li><?php echo anchor('member/event/edit/' . $event->id, e($event->title)); ?> -  <span class="imageSuccess">modified: </span><?php echo date('Y-m-d', strtotime(e($event->modified))); ?></li>
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	</div>
</div>