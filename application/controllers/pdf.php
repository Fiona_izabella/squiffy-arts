<?php
class Pdf extends Frontend_Controller {

    public function __construct(){
        parent::__construct();
        
    }

    public function index(){

     //load the dompdf helper. also need the file helper.
     $this->load->helper(array('dompdf', 'file'));
  
     //get all events and store in $events
        $this->event_m->pdf_events();
        $data['events'] = $this->event_m->get();  
       
        $html = $this->load->view('pdfprintout', $data, true);
    	
        pdf_create($html, 'squiffyArts events');
     
   }




}