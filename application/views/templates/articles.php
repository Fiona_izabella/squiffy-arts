<!-- Main content -->
<div class="row-fluid">
 <div class="span12">

	<div class="row-fluid">
		<div class="span12">
			<?php if($pagination): ?>
				<section class="pagination"><?php echo $pagination; ?></section>
			<?php endif; ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span4 offset1">		 		
			<?php echo anchor('article/add', '<h2 class="addArticle"><i class="icon-plus"></i>Add article</h2>'); ?>		
		</div>
	</div>

	<?php if (count($articles)): foreach ($articles as $article): ?>

		<div class="row-fluid">
			<article class="span10 offset1 news">
				<!--print to page using get_excerpt() funciton created in CMS_helper.php-->
				<?php echo get_excerpt($article); ?><hr>
			</article>

		</div>					
	<?php endforeach; endif; ?>


</div>
</div>