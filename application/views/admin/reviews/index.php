<div class = "row-fluid">
<div class = "span10 offset1">

<section>
	<h2>Reviews</h2>
	<?php echo anchor('admin/reviews/edit', '<i class="icon-plus"></i> Add a review','class="addArticle"'); ?>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>date</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
<?php if(count($reviews)): foreach($reviews as $review): ?>	
		<tr>
			<td><?php echo anchor('admin/reviews/edit/' . $review->id, $review->title); ?></td>
			<td><?php echo $review->create; ?></td>
			<td><?php echo btn_edit('admin/reviews/edit/' . $review->id); ?></td>
			<td><?php echo btn_delete('admin/reviews/delete/' . $review->id); ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any reviews.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
</section>

</div>
</div>