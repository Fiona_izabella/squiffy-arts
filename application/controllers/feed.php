<?php
class Feed extends Frontend_Controller {

    public function __construct(){
        parent::__construct();


        //load the helpers and model
         $this->load->helper('xml');  
         $this->load->helper('text');  
         $this->load->model('feeds_m', 'feeds');
         //With the second parameter, we assign our model to a different object name
        
    }


    function index()
    //index is the method called by default
	{
		$data['feed_name'] = 'squiffy arts'; // my website
		$data['encoding'] = 'utf-8'; // the encoding
        $data['feed_url'] = 'http://localhost:8888/squiffyArts27/feed'; // the url to my feed
        $data['page_description'] = 'Squiffy arts latest news'; //description
        //['page_description'] gets passed as $page_description so can (echo $page_description)
        $data['page_language'] = 'en-en'; // the language
        $data['creator_email'] = 'fionka78@hotmail.com'; // my email
        $data['posts'] = $this->feeds->getFeeds(10); 
        //note we are calling out model 'feeds_m'. we are getting the results from the getFeeds function
        //and storing it into the $posts array (as ['posts'] becomes $posts) and we can
        //foreach loop through the $posts.eg $post[date] and $post[text]
        header("Content-Type: application/rss+xml"); // important!

        $this->load->view('rss', $data);
	}

}