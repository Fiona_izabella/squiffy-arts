<?php
class Rss extends Admin_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('feeds_m');
	}

	public function index ()
	{
		// Fetch all rss feeds
		$this->data['feeds'] = $this->feeds_m->get();
		// Load view
		$this->data['subview'] = 'admin/rssfeed';
		$this->load->view('admin/_layout_main', $this->data);
	}


    public function addFeed(){

        // firstset up the rules made in feeds_m
        $rules = $this->feeds_m->rules;
        $this->form_validation->set_rules($rules);
        
        // Process the form
        //if passes validation call the savefeeds() method 
        if ($this->form_validation->run() == TRUE) {
            $this->feeds_m->savefeeds();
        }
 

         //get feeds from database and pass to view
        $this->data['feeds'] = $this->feeds_m->get();
        $this->data['subview'] = 'admin/rssfeed';
        $this->load->view('admin/_layout_main', $this->data);


    }

	
	public function delete ($id)
	{
		$this->feeds_m->delete($id);
		redirect('admin/rss');
	}

}