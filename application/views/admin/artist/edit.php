<div class = "row-fluid">
<div class = "span9 offset1">
<div id="top"></div>
<!--if no id then print "create a new artist" otherwise print "edit artist' followed by their first name-->
<h3><?php echo empty($artist->id) ? 'Add a new artist' : 'Edit artist ' . $artist->fname; ?></h3>
<?php $attributes = array('id' => 'myform');//add attributes to the form
echo form_open("",$attributes);?>
<!--create table which includes the original values and set up a separate error message for each form_input-->
<div id="inputs">
<table class="table">
	<tr>
		<td>first name</td>
		<td><?php echo form_input('fname', set_value('fname', $artist->fname), 'id="fname" title="Name should be the same as slug."'); ?><?php echo form_error('fname', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
	<tr>
		<td>last name</td>
		<td><?php echo form_input('lname', set_value('lname', $artist->lname), 'title="Should be at least 8 characters."'); ?><?php echo form_error('lname', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>Slug</td>
		<td><?php echo form_input('slug', set_value('slug', $artist->slug), 'title="Slug should be the same as name."'); ?><?php echo form_error('slug', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>email</td>
		<td><?php echo form_input('email', set_value('email', $artist->email),'title="Must be a valid email"'); ?><?php echo form_error('email', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>	
	<tr>
		<td>profile</td>
		<td><?php echo form_textarea('profile', set_value('profile', $artist->profile), 'class="ckeditor" title="profile must not exceed 1200 characters"'); ?><?php echo form_error('profile', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>

	<tr>
		<td>facebook</td>
		<td><?php echo form_input('facebook', set_value('facebook', $artist->facebook)); ?><?php echo form_error('facebook', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>twitter</td>
		<td><?php echo form_input('twitter', set_value('twitter', $artist->twitter)); ?><?php echo form_error('twitter', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>blog</td>
		<td><?php echo form_input('blog', set_value('blog', $artist->blog)); ?><?php echo form_error('blog', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>website</td>
		<td><?php echo form_input('website', set_value('website', $artist->website)); ?><?php echo form_error('website', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	
	<tr>
		<td></td>
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>

	</tr>

	
	<tr>
		<td></td>
		<td></td>
	</tr>

	</div>
</table>

<?php echo form_close();?>

<div>
<h2>Your Images</h2>
</div>
<!--need to set $id to null if we are adding a new artist with no previous $id-->
<?php //if (!isset($artist->id))  {
//$artist->id = NULL;
//$currentImage = "you have no image uploaded";
//} else {
//$currentImage = "your current image";
//}; ?>

<!-- <h3>Image one</h3>
<p><?php //echo $currentImage;?></p>
<?php //$img1 = $artist->img_source1; ?>
<p><?php //echo img('uploads/thumbs/'.$img1); ?></p>
<br /><br />
<hr> -->

<!--need to set $id to null if we are adding a new artist with no previous $id-->
<h3>Image one</h3>
<?php if (!isset($artist->id)):?>
<?php $artist->id = NULL;
$currentImage = "You have no image uploaded";?>
<p><?php echo $currentImage;?></p>

<?php elseif (isset($artist->id) && !isset($artist->img_source1)):
$currentImage = "You have no image uploaded";?>
<p><?php echo $currentImage;?></p>

<?php else: 
$currentImage = "Your current image";?>
<p><?php echo $currentImage;?></p>
<?php $img1 = $artist->img_source1; ?>
<div class="row-fluid">
<div class="span4 offset1"><?php echo img('uploads/thumbs/'.$img1); ?></div>
</div>
<?php endif; ?>

<br /><br />
<hr>

<!--need to set $id to null if we are adding a new artist with no previous $id-->
<h3>Image two</h3>
<?php if (!isset($artist->id)):?>
<?php $artist->id = NULL;
$currentImage = "You have no image uploaded";?>
<p><?php echo $currentImage;?></p>

<?php elseif (isset($artist->id) && !isset($artist->img_source2)):
$currentImage = "You have no image uploaded";?>
<p><?php echo $currentImage;?></p>

<?php else: 
$currentImage = "Your current image";?>
<p><?php echo $currentImage;?></p>
<?php $img2 = $artist->img_source2; ?>
<div class="row-fluid">
<div class="span4 offset1"><?php echo img('uploads/thumbs/'.$img2); ?></div>
</div>
<?php endif; ?>


<br /><br />
<hr>

<!--need to set $id to null if we are adding a new artist with no previous $id-->
<h3>Image three</h3>
<?php if (!isset($artist->id)):?>
<?php $artist->id = NULL;
$currentImage = "You have no image uploaded";?>
<p><?php echo $currentImage;?></p>

<?php elseif (isset($artist->id) && !isset($artist->img_source3)):
$currentImage = "You have no image uploaded";?>
<p><?php echo $currentImage;?></p>

<?php else: 
$currentImage = "Your current image";?>
<p><?php echo $currentImage;?></p>
<?php $img3 = $artist->img_source3; ?>
<div class="row-fluid">
<div class="span4 offset1"><?php echo img('uploads/thumbs/'.$img3); ?></div>
</div>
<?php endif; ?>

<br /><br />
<hr>

<p><?php echo anchor('admin/artist','back to artists', 'class="btn btn-primary btn-custom"'); ?></p>
<br />
<p><a href="<?=current_url();?>#top" class="btn btn-primary btn-custom">Go to top of page</a></p>


</div>
</div>