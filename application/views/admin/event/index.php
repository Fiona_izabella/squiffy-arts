<div class = "row-fluid">
<div class = "span10 offset1">
<section>
	<h2>Events</h2>
	<?php echo anchor('admin/event/edit', '<i class="icon-plus"></i> Add an event','class="addArticle"'); ?>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>created</th>
				<th>date</th>
				<th>Edit</th>
				<th>Add/Edit image</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
<?php if(count($events)): foreach($events as $event): ?>	
		<tr>
			<td><?php echo anchor('admin/event/edit/' . $event->id, $event->title); ?></td>
			<td><?php echo $event->created; ?></td>
			<td><?php echo $event->date; ?></td>
			<td><?php echo btn_edit('admin/event/edit/' . $event->id); ?></td>
			<td><?php echo btn_edit('admin/event/do_upload/' . $event->id); ?></td>
			<td><?php echo btn_delete('admin/event/delete/' . $event->id); ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any events.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
</section>

</div>
</div>