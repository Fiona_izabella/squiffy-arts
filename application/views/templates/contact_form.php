<!--subnav-->
   <div class="row-fluid">
        <div class="span2 offset3"><h7 class=""><?php echo anchor('surroundingarea', 'area'); ?></h7></div>
        <div class="span2"><h7 class="wider"><?php echo anchor('map', 'map'); ?></h7></div>
        <div class="span2"><h7 class="contact"><?php echo anchor('email', 'email us'); ?></h7></div>   
   </div> 
<!--end of subnav-->
<!--content-->

<div class="row-fluid">
  <section class="span10 offset1">

	<h1>contact form</h1>

		<div id="newsletterForm">
		<!--echo out the flasdata 'message' if it is not blank-->
			<p class = "emailflashdata">
			<?php if ($this->session->flashdata('message') != ''): 
	        echo $this->session->flashdata('message'); 
	        endif; ?>
	        </p>

			<?php echo form_open('email/send')?>
			<?php
			$name_data = array(
			 'name' => 'name',
			 'id' => 'name',
			 'value' => set_value('name'),//to remember what value the user put in
			);
			?>
			<p class="emailForm"><label for="name">Name: </label><?php echo form_error('name', '<div class="errorEmail">', '</div>'); ?></p>
			<p><?php echo form_input($name_data);?></p>
			<p><label for="email">Email: </label><?php echo form_error('email', '<div class="errorEmail">', '</div>'); ?></p>
			<p><?php echo form_input('email', set_value('email'));?></p>
			<p>
			<label for="message">Message: </label>
			<?php echo form_textarea('message', set_value('message'),'class="ckeditor"'); ?><?php echo form_error('message', '<div class="errorEmail">', '</div>'); ?>
			</p>
			<p>
			<?php echo form_submit('submit', 'submit message', 'class="btn-custom"')   ?>
			</p>
			<?php echo form_close(); ?>
						

		</div>

 </section>
</div>
    <!--end of content-->