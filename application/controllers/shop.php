<?php
class Shop extends Frontend_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('shop_m');
		
	}

	function index(){ 
		  
            //add page title
			$data['meta_title'] = "shop";
			//need an if statement for value of $category to control the products displayed on each page
            $shoppage = $this->uri->segment(2);
	               
	               if($shoppage =="souvenir"){

				   $category = 'souvenir';

			       } elseif($shoppage =="poster"){
	 
	               $category = 'poster';
			       }

			       elseif($shoppage =="art"){

	               $category = 'art';

			       } else {

			       $category = null;
			       }

			$data['heading'] = $category;       

		    $data['products'] = $this->shop_m->retreive_products($category); // Retrieve an array with all products
		     //pass the main menu
		    $data['menu'] = $this->page_m->get_nested();
		    /**
		     * give the $content variable this value and load the view
		     *Inside 'cart/index' we have...$this->view($contents);
		     */
		    $data['contents'] = 'shop/pruducts';
            $this->load->view('shop/index', $data);
	}


    /*add to cart. get id and quantity of product*/
	function add() {

		    //get product info from $_POST['id'];
		    $product = $this->shop_m->get($this->input->post('id'));
		    //get quantity value  from $_POST['quantity'];
			$quantity = $this->input->post('quantity');
			//need to get the value of the category of the added value and use it as the value of $originalPage
			$originalPage = $product->category;
		
		    //create an array with product id, quantity, and details and insert into cart session
		    $insert = array(
			'id' => $this->input->post('id'),
			'qty' => $quantity,
			'price' => $product->price,
			'name' => $product->name,
			'category' => $product->category,
			'image' => $product->image
		     );
		
		
			$this->cart->insert($insert);
			redirect('shop/'.$originalPage);
		 
	}

/**
 * [This function removes an item form the cart]
 * @param  [int] $rowid [Each array in cart has a unique row id]
 * @return [session]        [updates the session cart]
 */
	function remove($rowid) {

        //use the value of $rowid to find the quantity value and minus 1 from that value.
      	$cart = $this->cart->contents();
      	$product = $cart[$rowid];
      	$category = $product['category'];
      	$q = $product['qty'];

		$data = array(
        'rowid' => $rowid,
		'qty' => $q-1,
		'category' => $category
		);	
		$this->cart->update($data);
		redirect('shop/'.$category);
		
	}

	function emptyCart(){
		//die();
	  $this->cart->destroy();
	  //reload the page after clearing the cart
      $category = $this->uri->segment(2);
      $data['heading'] = "Menu"; 
      $data['meta_title'] = "shop";
      $data['products'] = $this->shop_m->retreive_products($category); // Retrieve an array with all products
	  $data['menu'] = $this->page_m->get_nested();
	  $data['contents'] = 'shop/pruducts';//give the $content vairable this value and load the view
      $this->load->view('shop/index', $data);
     

	}

	function pay() {


		    $data['meta_title'] = "shop";
		    $data['menu'] = $this->page_m->get_nested();
            $data['contents'] = 'shop/pay';
            $this->load->view('shop/index', $data);

        }

     function paynow($order_id = NULL) {

     	    //need to clear cart and load receipt page
     	    //clear cart in page when select "emptycart link"
     	    //need to insert into database cart contents
     	   
     	   //get values from form and $cartsession
           // firstset up the rules made in shop model
           $rules = $this->shop_m->rules;
           $this->form_validation->set_rules($rules);

            if ($this->form_validation->run() == TRUE) {

           $data['fname'] = $this->input->post('fname');
           $data['lname'] = $this->input->post('lname');
           $data['email'] = $this->input->post('email');
           $data['address'] = $this->input->post('address');
           $data['phonenumber'] = $this->input->post('phonenumber');
           $data['amount_paid'] = $this->cart->total();
           $data['numbItems'] = $this->cart->total_items();
            
            $orderIdsuccess = $this->shop_m->saveSales($data, $order_id);
            //save the $data into the database
            //this returns an $order_id which is passed to the variable  $orderIdsuccess
            // $orderIdsuccess is passed to saveOrderDetails() function

            //save $cart session into $cart array
            $cart = $this->cart->contents();
            //pass $cart array and $orderIdsuccess value into saveOrderDetails() function

            $this->shop_m->saveOrderDetails($cart, $orderIdsuccess);

            $data['emailConfirm'] = $data['email'] = $this->input->post('email');
            $this->session->set_flashdata("message", $data);
            redirect('shop/receipt');

          	}

          	else{

            $data['meta_title'] = "shop";
		    $data['menu'] = $this->page_m->get_nested();
            $data['contents'] = 'shop/pay';
            $this->load->view('shop/index', $data);


          	}

     }

        function receipt() {
            $data['meta_title'] = "receipt";
        	$data['contents'] = 'shop/paynow';
            $this->load->view('shop/index_payment', $data);



        }

	


}//end of class
