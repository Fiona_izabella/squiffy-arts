<div class = "row-fluid">
<div class = "span10 offset1">

<section>
	<h2>Subscriptions</h2>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Created</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
<?php if(count($subscriptions)): foreach($subscriptions as $subscribe): ?>	
		<tr>
			<td><?php echo anchor('admin/subscriptions/edit/' . $subscribe->id, $subscribe->name); ?></td>
			<td><?php echo anchor('admin/subscriptions/edit/' . $subscribe->id, $subscribe->email); ?></td>
			<td><?php echo $subscribe->created; ?></td>
			<td><?php echo btn_edit('admin/subscriptions/edit/' . $subscribe->id); ?></td>
			<td><?php echo btn_delete('admin/subscriptions/delete/' . $subscribe->id); ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any subscriptions.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
</section>

</div>
</div>