<h1 class="centre">check out our scrolling gallery </h1>
<h3><?php echo anchor('gallery/index.php', 'hidden gallery <---')  ;?></h3>
<div id="container_showreel">

	<div class="row-fluid">
		<div class="span1">
			<div id="showreelnav">
				<div class="navBtns"><a href="#" id="s1"><img src="assets/images/parallax/parralaxmenufiona.png"/></a></div>
				<div class="navBtns"><a href="#" id="s2"><img src="assets/images/parallax/parralaxmenubridie.png"/></a></div>
				<div class="navBtns"><a href="#" id="s3"><img src="assets/images/parallax/parralaxmenumiaa.png"/></a></div>
				<div class="navBtns"><a href="#" id="s4"><img src="assets/images/parallax/parralaxmenudan.png"/></a></div>
			</div>

		</div><!--end of span1-->

		<div class="span11">

			<div id="p1" data-speed="10" class="background">

				<div class="text">
					<img src="assets/images/parallax/parralaxmenufiona.png" alt="image of legs with red tights"/>
					The images transform the seemingly mundane into explorations of colour, texture and form. The lack of human presence creates a sense of eerie absence which suggests a narrative, opening up the image to interpretation by the viewer.
					Through concentration on light and pattern, the pictures have a painterly quality and draw attention to the habitual surroundings that we pass through everyday.
				</div>
				<div class="pic" data-speed="-2" data-ypos="50" data-xpos="70%"></div>
			</div>


			<div id="p2" data-speed="10" class="background">
				<div class="text">
					<img src="assets/images/parallax/parralaxmenubridie.png" alt="image of theatre perfomance"/>
					Bridie has performed across the globe and always likes to get people involved in her productions. She has a vast portfolio and commissions behind her and never says no to a challenge. Her work deals with issues in society and her work always has an important message. Recently she performed in the Hackney playhouse with children from the local primary school. Hackney have commisioned her to do another piece with children in the future.
				</div>
				<div class="pic" data-speed="3" data-ypos="700" data-xpos="70%"></div>
			</div>

			<div id="p3" data-speed="10" class="background">
				<div class="text">
					<img src="assets/images/parallax/parralaxmenumiaa.png" alt-"image of demolished ceiling"/>
					Miaaa paints decrepid and interesting surroundings turning what most consider ugly into something beautiful. Her extraordinarily realistic painting style makes the viewer believe they are looking at a photograph until they insect closely and see the brushstrokes.
					Miaaa studied photography and went on to do an Ma in fine art paining.
				</div>
				<div class="pic" data-speed="2" data-ypos="1400" data-xpos="30%"></div>
			</div>

			<div id="p4" data-speed="10" class="background">
				<div class="text">
					<img src="assets/images/parallax/parralaxmenudan.png" alt="image of popart poster"/>
					Dan is an upcoming writer whose work includes poems, short stories and science fiction novels.
					Dan’s work is far from the conventional and reflects his roots in science where he has an uncanny talent for seeing beyond the skin of the world that exists around us, into the hidden secrets of the world as a complex machine making you think what or who is really pulling the strings.
				</div>
				<div class="pic" data-speed="-1" data-ypos="-2700" data-xpos="65%"></div>

			</div>	

		</div>

	</div>
	<p class="clearfix"></div>


