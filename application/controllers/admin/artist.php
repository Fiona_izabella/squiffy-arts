<?php
class Artist extends Admin_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('artist_m');
	}

	public function index ()
	{
		// Fetch all artists
		$this->data['artists'] = $this->artist_m->get();
		
		// Load view
		$this->data['subview'] = 'admin/artist/index';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function edit ($id = NULL)
	{
		// Fetch an artist or set a new one
		if ($id) {
			$this->data['artist'] = $this->artist_m->get($id);
			count($this->data['artist']) || $this->data['errors'][] = 'artist could not be found';
		}
		else {
			$this->data['artist'] = $this->artist_m->get_new();
		}
		
		// Set up the form
		$rules = $this->artist_m->rules;
		$this->form_validation->set_rules($rules);
		
		// Process the form using codigniter validation
		if ($this->form_validation->run() == TRUE) {
			$data = $this->artist_m->array_from_post(array(
				'fname', 
				'lname',
				'slug', 
				'profile', 
				'email',
				'facebook',
				'twitter',
				'blog',
				'website'
				
			));
			$this->artist_m->saveArtist($data, $id);
			redirect('admin/artist');
		}
		
		// Load the view and success message which displays upload success data
		$this->data['success'] = 'uploadstate';
		$this->data['subview'] = 'admin/artist/edit';
		$this->load->view('admin/_layout_main', $this->data);
	}


    //get artists data and load the editImage view
	public function editImage ($id = NULL)
	{
		// Fetch an artist or set a new one
		if ($id) {
			$this->data['artist'] = $this->artist_m->get($id);
			count($this->data['artist']) || $this->data['errors'][] = 'artist could not be found';
		}
		else {
			$this->data['artist'] = $this->artist_m->get_new();
		}
		
		// Load the view and success message
		$this->data['success'] = 'uploadstate';
		$this->data['subview'] = 'admin/artist/editImage';
		$this->load->view('admin/_layout_main', $this->data);
	}


	public function delete ($id)
	{
		$this->artist_m->delete($id);
		redirect('admin/artist');
	}

     //upload function using the field name as a parameter
	function do_upload($field_name)
	{    
 
      	/**
      	 * Set config values, load the upload library, if upload fails then display upload errors inside a flashdata session
      	 * find the $page_id value from the url segment and redirect to current page.
      	 * Else upload image, create a thumbnail using the image_lib library, insert image into the database coloumn which
      	 * corressponds to the function parameter $fieldname and display upload data success message using session flashdata.
      	 */
      
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
      
         $this->load->library('upload', $config);


         if ( ! $this->upload->do_upload($field_name)){	

			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata($error);
            $page_id= $this->uri->segment(5);
            redirect('admin/artist/editImage/' . $page_id);
                   

		} else {

		$this->upload->do_upload($field_name);
		$image_data = $this->upload->data();

		
			// Thumbnail
            $config['image_library'] = 'gd2';
            $config['new_image'] = './uploads/thumbs/';//the new image
            $config['source_image'] = $image_data['full_path'];//this is the source
            $config['maintain_ratio'] = FALSE;
            $config['width'] = 200;
            $config['height'] = 143;

             $this->load->library('image_lib', $config);
             $this->image_lib->resize();

            $this->artist_m->insert_image($field_name, $this->upload->data());
            $data = array('upload_data' => $this->upload->data());
            $this->session->set_flashdata('message', $data);
			$page_id= $this->uri->segment(5);	
			redirect('admin/artist/editImage/' . $page_id);		        

		}


	}
	


}//end of class



