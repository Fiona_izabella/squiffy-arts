<div class = "row-fluid">
<div class = "span10 offset1">

<h3>Add a new feed</h3>
<?php echo form_open('admin/rss/addFeed'); ?>
<table class="table">

	<tr>
		<td>title</td>
		<td><?php echo form_input('title'); ?><?php echo form_error('title', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>message</td>
		<td><?php echo form_textarea('text'); ?><?php echo form_error('text', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>
	</tr>
	
</table>
<?php echo form_close();?>

<hr>

<table class="table table-striped">
		<thead>
			<tr>
				<th>Title</th>
				<th>text</th>
				<th>date</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
<?php if(count($feeds)): foreach($feeds as $feed): ?>	
		<tr>
		
			<td><?php echo $feed->title; ?></td>
			<td><?php echo $feed->text; ?></td>
			<td><?php echo $feed->date; ?></td>
			<td><?php //echo btn_edit('admin/article/edit/' . $article->id); ?></td>
			<td><?php echo btn_delete('admin/rss/delete/' . $feed->id); ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any feeds.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>


</div>
</div>