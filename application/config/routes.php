<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'page';
$route['404_override'] = 'page';
$route['article/(:num)/(:any)'] = 'article/index/$1/$2';
$route['reviews/(:num)'] = 'reviews/index/$1';
$route['artist/(:num)/(:any)'] = 'artist/index/$1/$2';
$route['supportus'] = 'supportus/index';
$route['shop/add'] = 'shop/add';
$route['shop/pay'] = 'shop/pay';
$route['shop/paynow'] = 'shop/paynow';
$route['shop/emptyCart'] = 'shop/emptyCart';
$route['shop/receipt'] = 'shop/receipt';
$route['shop/delete/(:any)'] = 'shop/delete/$1';
$route['shop/remove/(:any)'] = 'shop/remove/$1';
$route['shop/(:any)'] = 'shop/index';


/* End of file routes.php */
/* Location: ./application/config/routes.php */