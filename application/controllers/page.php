<?php

class Page extends Frontend_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('page_m');
         $this->load->model('event_m');
        $this->load->model('artist_m');
        $this->load->spark('console/0.7.0');
    }
    /**
     *Function which fetches the appropriate template 'view' and page contents according
     *to its slug. It uses the template to find the correct method (within this class) for each page.
     *The method...for example,"function _articles()" has its own logic which interacts with the model and
     *the database to get the coresspoding data for each page
     */

    public function index() {

        /*Fetch the page template using page models get_by() method. Run the data through the XSS filter by adding TRUE as second parameter*/
        $this->data['page'] = $this->page_m->get_by(array('slug' => (string) $this->uri->segment(1)), TRUE);
        //count($this->data['page']) || show_404(current_url());///use this to redirect to 404 page
        //redirect to welcome page if page is not found__* I have chosen this methosd instead as it seems tidier
        count($this->data['page']) || redirect('welcomepage');
        //use add_meta_title() created in CMS_helper to pass the page title
        add_meta_title($this->data['page']->title);
        
        // Fetch the page data by calling the appropriate method/function
        $method = '_' . $this->data['page']->template;
        if (method_exists($this, $method)) {
            $this->$method();
        }
        else {
            log_message('error', 'Could not load template ' . $method .' in file ' . __FILE__ . ' at line ' . __LINE__);
            show_error('Could not load template ' . $method);
        }
        
         //load the view
        $this->data['subview'] = $this->data['page']->template;
        $this->load->view('_main_layout', $this->data);
        //console_log('This is my log message', 'log', FALSE);
    }

/*-------------templates-----------------------------------------------------------*/

/*------*template for subscription----*/
    public function addSubs(){
        //1- set up rules for vaidation. get rules from model and pass to the form validation
        $rules = $this->subscription_m->rules;
        $this->form_validation->set_rules($rules);

         // Process the form
        //if passes validation
         if ($this->form_validation->run() == TRUE) {
        $now = date('Y-m-d H:i:s');          
        $data = array(
               'name' => $this->input->post('name'),
               'email' => $this->input->post('email'),
               'created' => $now
            );
           //insert into database and redirect to current page
            $this->db->insert('subscriptions', $data);
            redirect(base_url("welcomepage"));
            //redirect(current_url());
           

          } else {

            //redirect(current_url());
            redirect(base_url("welcomepage"));
         
         }

    }
    
    private function _page(){
        /**
         * method can be blank as all the data needed gets passed from the $page variable
         * initated in the index method. however the function must still exist.
         */
       
    }


    private function _about(){
        /**
         * method can be blank as all the data needed gets passed from the $page variable
         * initated in the index method. however the function must still exist.
         */
       
    }

    
    private function _welcomepage(){
       /**
         * method can be blank as all the data needed gets passed from the $page variable
         * initated in the index method. however the function must still exist.
         */
       
    }

     private function _contact(){
        /**
         * method can be blank as all the data needed gets passed from the $page variable
         * initated in the index method. however the function must still exist.
         */
       
    }

     private function _accordion(){
         /**
         * method can be blank as all the data needed gets passed from the $page variable
         * initated in the index method. however the function must still exist.
         */      
    }
    
    
    private function _shop(){
       /**
         * method can be blank as all the data needed gets passed from the $page variable
         * initated in the index method. however the function must still exist.
         * The child shop pages get their data from the Shop Controller class.
         */
       
    }
    

    private function _articles(){     
        // Count all articles     
        $count = $this->db->count_all_results('articles');
        
        // Set up pagination
        $perpage = 4;
        if ($count > $perpage) {
            $this->load->library('pagination');
            $config['base_url'] = site_url($this->uri->segment(1) . '/');
            $config['total_rows'] = $count;
            $config['per_page'] = $perpage;
            $config['uri_segment'] = 2;
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(2);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        
        // Fetch articles, order by the date created, limit number per page for pagination
         $this->db->order_by('created', 'desc');
         $this->db->limit($perpage, $offset);
         $this->data['articles'] = $this->article_m->get();
      }


    private function _add_comment(){
        /**
         * method can be blank as all the data needed gets passed from the methods in the 
         * above _articles() method.
         */

    }

    private function _event(){       
              
        // Count all events   
       $count = $this->db->count_all_results('events');
       //use a switch statement to determine the day from the url segment
        $m = $this-> uri->segment(1);

        switch ($m) {
        case 'friday':
        $this->event_m->friday_date();
        break;

        case 'saturday':
        $this->event_m->sat_date();
        break;

        case 'sunday':
        $this->event_m->sun_date();
        break;
    
        }
        
        // Fetch events
        $this->data['events'] = $this->event_m->get();   
    }

    private function _eventMenu(){
        /**
         * method can be blank as all the data needed gets passed from the methods in the 
         * above _event() method.
         */
         
    }

    private function _shopMenu(){
         /**
         * method can be blank as all the data needed gets passed from the $page variable
         * initated in the index method and the Shop controller. however the function must still exist.
         */
         
    }


    private function _artistspage(){
              
        // Count all artists
        $count = $this->db->count_all_results('artists');
        
        // Set up pagination
        $perpage = 8;
        if ($count > $perpage) {
            $this->load->library('pagination');
            $config['base_url'] = site_url($this->uri->segment(1) . '/');
            $config['total_rows'] = $count;
            $config['per_page'] = $perpage;
            $config['uri_segment'] = 2;
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(2);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        
        // Fetch artists
        $this->db->limit($perpage, $offset);
        $this->data['artists'] = $this->artist_m->get();
    }

   
/*-------------search functions-----------------------------------------------------------*/


    function autocomplete()
    {
       $keyword = $this->input->post('search');
       echo ($keyword);
        
    }





}//end of page class