<h3>Your total to pay</h3>
<?php if ($cart = $this->cart->contents()): ?>
	<h1>&pound;<?php echo $this->cart->total(); ?></h1>
<?php endif; ?>
<div class="row-fluid">

	<div class="span7">
		<h4>Procceed to secure payment.....</h4>
		<!--add attributes to form in array-->
		<?php $attributes = array('class' => 'contact_formProduct', 'name' => 'contact_formProduct'); ?>
        <?php echo form_open('shop/paynow',$attributes ); ?>
            <ul>
                <li>
                     <h2>Enter your details</h2>
                     <span class="required_notification">* Denotes Required Field</span>
                </li>
                <li>
                <?php echo form_label('first name:', 'fname');
                  $data = array(
                      'name'        => 'fname',
                      'placeholder' => 'john',
                      'required' => 'required'
                    );
                   
                   echo form_input($data);
                   echo form_error('fname', '<div class="errorArticle">', '</div>'); ?> 
                </li>
                <li>
                     <?php echo form_label('last name:', 'lname');
                       $data = array(
                      'name'        => 'lname',
                      'placeholder' => 'Doe',
                      'required' => 'required'
                    );
                   
                   echo form_input($data);
                   echo form_error('lname', '<div class="errorArticle">', '</div>'); ?>
                </li>
                <li>
                  <?php echo form_label('Email:', 'email');
                       $data = array(
                      'name'        => 'email',
                      'type' => 'email',
                      'placeholder' => 'john_doe@example.com',
                      'required' => 'required'
                    );
                   
                   echo form_input($data);
                   echo form_error('email', '<div class="errorArticle">', '</div>'); ?>
                    <span class="form_hint">Proper format "name@something.com"</span>
                </li>
                 <li>
                    <?php echo form_label('Address:', 'address');
                       $data = array(
                      'name'        => 'address',
                      'placeholder' => '10 john lane',
                      'cols'       => '60',
                      'rows'    => '3',
                      'title'  => 'must be your posting address',
                      'required' => 'required'
                    );
                   
                   echo form_textarea($data);
                   echo form_error('address', '<div class="errorArticle">', '</div>'); ?>

                   <span class="form_hint">Proper format "This must be your posting address"</span>
                </li>
                <li>
                   <?php echo form_label('Phone number:', 'phonenumber'); ?>
                    <input type='tel' name="phonenumber" pattern='^(07[\d]{8,12}|447[\d]{7,11})$' title='Number Format: 07646300190' required> 
                   <?php echo form_error('phonenumber', '<div class="errorArticle">', '</div>'); ?>
                    <span class="form_hint">Proper format "07646300190"</span>
                </li>      
                <li>
                	<button class="btn-custom btn-empty btn-pay" type="submit">Purchase</button>
                </li>
             </ul>

     <?php echo form_close();?>

	</div>
</div>

<div class="row-fluid">
	<div class="span6">
		<h4>oR... <?php echo anchor('shop', 'return to cart') ;?></h4>
	</div>
</div>
