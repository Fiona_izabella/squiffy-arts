<div class = "row-fluid">
<div class = "span9 offset1">
<div id="top"></div>
<!--if no id then print "create a new artist" otherwise print "edit artist' followed by their first name-->
<h3><?php echo empty($artist->id) ? 'Add a new Image' : 'Edit images for ' . $artist->fname; ?></h3>

<h2>Upload your Images</h2>

<!--upload FIRST IMAGE-->
<!--need to set $id to null if we are adding a new artist with no previous $id-->
<?php if (!isset($artist->id))  {
$artist->id = NULL;
$currentImage = "you have no image uploaded";
} else {
$currentImage = "your current image";
}; ?>

<?php echo form_open_multipart('member/artist/do_upload/1/' . $artist->id);?> 
<h3>upload image one</h3>
<p><?php echo $currentImage;?></p>
<?php $img1 = $artist->img_source1; ?>
<p><?php echo img('uploads/thumbs/'.$img1); ?></p>
<br /><br />
<?php echo form_upload('1', set_value('userfile1', $artist->img_source1)); ?>
<br /><br />
<input type="submit" value="upload" />
<?php echo form_close();?>
<hr>

<!--upload SECOND IMAGE-->
<!--need to set $id to null if we are adding a new artist with no previous $id-->
<?php if (!isset($artist->id))  {
$artist->id = NULL;
$currentImage = "you have no image uploaded";
} else {
$currentImage = "your current image";
}; ?>
<?php echo form_open_multipart('member/artist/do_upload/2/' . $artist->id);?> <td>
<h3>upload image two</h3>
<p><?php echo $currentImage;?></p>
<?php $img2 = $artist->img_source2; ?>
<p><?php echo img('uploads/thumbs/'.$img2); ?></p>
<br /><br />
<?php echo form_upload('2', set_value('userfile2', $artist->img_source2)); ?>
<br /><br />
<input type="submit" value="upload" />
<?php echo form_close();?>
<hr>

<!--upload THIRD IMAGE-->
<!--need to set $id to null if we are adding a new artist with no previous $id-->
<?php if (!isset($artist->id))  {
$artist->id = NULL;
$currentImage = "you have no image uploaded";
} else {
$currentImage = "your current image";
}; ?>
<?php echo form_open_multipart('member/artist/do_upload/3/' . $artist->id);?> <td>
<h3>upload image three</h3>
<p><?php echo $currentImage;?></p>
<?php $img3 = $artist->img_source3; ?>
<p><?php echo img('uploads/thumbs/'.$img3); ?></p>
<br /><br />
<?php echo form_upload('3', set_value('userfile3', $artist->img_source3)) ?>
<br /><br />
<input type="submit" value="upload" />
<?php echo form_close();?>
<hr>

<!--print out flashdata which holds the errors and upload image success data-->
<div id="infoMessage"><?php echo $this->session->flashdata('error');?></div>
<hr>
<div id="successMessage">
		 <?php if($this->session->flashdata('message')){ ;?>	

		 	<?php $data = $this->session->flashdata('message'); ?>						
			<ul class ="imageSuccess">
			<h5>Welldone your image uploaded </h5>
			<li>Your image data: </li>
			<?php foreach ($data{'upload_data'} as $item => $value):?>
			<li><?php echo $item;?>: <?php echo $value;?></li>
			<?php endforeach; ?>
			</ul>
	
		<?php } ;?>

		 </div>
		 <br />
		<p><?php echo anchor('member/artist','back to artists', 'class="btn btn-primary btn-custom"'); ?></p>
		<br />
		 <p><a href="<?=current_url();?>#top" class="btn btn-primary btn-custom">Go to top of page</a></p>

</div>
</div>