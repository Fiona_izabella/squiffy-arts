<div class = "row-fluid">
<div class = "span9 offset1">

<section>
	<h2>Order pages</h2>
	<p class="alert alert-info">Drag to order pages and then click 'Save'</p>
	<div id="orderResult"></div>
	<input type="button" id="save" value="Save" class="btn btn-primary btn-custom" />
</section>
<!--id $sortable variable is set and TRUE include the js files for the ui and nested sortable-->
<?php if(isset($sortable) && $sortable === TRUE): ?>
	   <script src="<?php echo site_url('assets/js/jquery-ui-1.9.1.custom.min.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/jquery.mjs.nestedSortable.js'); ?>"></script>
		<?php endif; ?>
<script>
/*script which calls the order_ajax function*/
$(function() {
	$.post('<?php echo site_url('admin/page/order_ajax'); ?>', {}, function(data){
		$('#orderResult').html(data);
	});

	$('#save').click(function(){
		oSortable = $('.sortable').nestedSortable('toArray');

		$('#orderResult').slideUp(function(){
			$.post('<?php echo site_url('admin/page/order_ajax'); ?>', { sortable: oSortable }, function(data){
				$('#orderResult').html(data);
				$('#orderResult').slideDown();
			});
		});
		
	});
});
</script>

</div>
</div>