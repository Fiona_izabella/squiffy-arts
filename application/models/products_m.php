<?php
class Products_m extends MY_Model
{
	protected $_table_name = 'products';
	protected $_order_by = 'id asc';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';//intval casts any input as an integar
	protected $_timestamps = FALSE;
	public $rules = array(
		'name' => array(
			'field' => 'name', 
			'label' => 'name', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		), 
		'price' => array(
			'field' => 'price', 
			'label' => 'price', 
			'rules' => 'trim|required|max_length[32]|numeric|xss_clean'
		), 
		'description' => array(
			'field' => 'description', 
			'label' => 'description', 
			'rules' => 'trim|required|max_length[255]'
		),
		'category' => array(
			'field' => 'category', 
			'label' => 'category', 
			'rules' => 'trim|required|max_length[25]'
		
		)
	);

	  /** [create a new empty object] */
	public function get_new ()
	{
		$product = new stdClass();
		$product->name = '';
		$product->price = '';
		$product->description = '';
		$product->category = '';
		$product->imgage = '';
		return $product;
	}

    //insert the product or update product
	public function save_product($data, $id = NULL){
		//if you pass an $id it will be an update, othrwise it will be an insert.
				
		// Insert
		if ($id === NULL) {
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();
		}
		// Update
		else {
			$filter = $this->_primary_filter;//filter the primary key
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}
		
		return $id;
	}
	

     //insert image into products database
	function insert_images($image_data = array()){
      $data = array(
          'image' => $image_data['file_name'],

           );
      
       $this ->db->where('id', $this->uri->segment(4));
       $this->db->update('products', $data);

   
	}



}