<!DOCTYPE html>
<html>
<head>
    <link id="stylesheet" href="<?php echo site_url('assets/css/pdf.css'); ?>" rel="stylesheet">
</head>
<body>
    <div class="container">

        <div id="printout">
            <h1>Events Print Out</h1>
            <?php //print_r($events);?>
            <div class="row-fluid">
                <section class="span10 offset2"> 

                    <?php 
                    if (count($events)): foreach ($events as $event): ?>

                    <?php $img = get_imageinfo($event);?>
                    <?php echo img('uploads/'.$img); ?>
                    <!--call method get_eventblock() form CMS_helper.php to output string to page-->
                    <?php echo get_eventblock($event); ?>
                    <?php echo get_moreinfo($event); ?>

                    <hr>

                <?php endforeach; endif; ?>

            </section>
        </div>
        <!--end of content-->
    </body>
    </html>