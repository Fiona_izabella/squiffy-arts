<?php 

class Migration_Create_Subscriptions extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			
		));
        
        $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('subscriptions');
	}

	public function down()
	{
		$this->dbforge->drop_table('subscriptions');
	}


}
