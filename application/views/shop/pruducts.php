<div class="row-fluid">

     <div class="span7 productsWrap">

          <div class="row-fluid">
                 <div class="span2 offset2"><h6><?php echo anchor('shop/poster','posters'); ?></h6></div>
                <div class="span2"><h6><?php echo anchor('shop/souvenir','souvenirs'); ?></h6></div>
                <div class="span2"><h6><?php echo anchor('shop/art','artwork'); ?></h6></div>  
          </div> 
          <!--end of subnav-->

   <!--load the view 'pruductslist.php'-->
  <?php $this->load->view('shop/pruductslist.php'); ?>


  </div><!--end of products wrap of span 7-->

     <div class="span5 cartMain">
     <!--if cart is not empty do the following-->
      <?php if ($cart = $this->cart->contents()): ?>
      <?php //print_r($cart);?>

      <div class="row-fluid">
      <div class="span12">

          <div class="cart">
          <table class="table">
             <h3>Shopping Cart</h3>
              <thead>
                 
                <th>Item Name</th>
                <th>quantity</th>
                <th>Price</th>
                <th>Remove</th>
               
              </thead>
                  <!--set $i variable with initial value of 1-->
                  <?php $i = 1; ?>
                  <?php foreach ($cart as $item): ?>
              <tr>
                  <td><?php echo $item['name']; ?></td>
                  <td><?php echo $item['qty']; ?></td>
                  <td>£<?php echo $item['subtotal']; ?></td>
                  <td class="remove">
                      <?php echo anchor('shop/remove/'.$item['rowid'],'X'); ?>
                  </td>               
              </tr>
                  <!--increment $i vlaue through each loop-->
                  <?php $i++; ?>
                  <?php endforeach; ?>
            <tr class="total">
                <td colspan="2"><strong>Total</strong></td>
                <td>£<?php echo $this->cart->total(); ?></td>
                <td></td>
            </tr>
        </table> 
   
             <?php echo anchor('shop/emptyCart','empty carT', 'class="btn-custom btn-empty"'); ?>  
             <?php echo anchor('shop/pay','proceed to checkout', 'class="btn-custom btn-empty"'); ?>   
    </div><!--end of cart class-->

     <?php else: 
        echo '<h3>.....Your cart is empty..</h3>'; ?>
 
     <?php endif; ?>

    </div>
    </div>

    </div><!--end of span5 cartMain-->
    </div><!--end of top row-fluid-->




