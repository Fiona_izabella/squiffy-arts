<h1>search results</h1>
<div class="row-fluid">
  <div class="span12">
    <h3 class="shiftRgt"><?php echo $errorMessage;?></h3>

    <?php 
//if $events array exists and is not empty, loop through the array.
    if (is_array($events) && !empty($events)): foreach ($events as $event): ?>

    <div class="row-fluid">
      <article class="span9 offset2">

        <h3>Event: <?php echo $event->title; ?> </h3>
        <p class="date">Taking place on: <?php echo $event->date; ?> </p>
        <p class="date">Time: <?php echo $event->time; ?> </p>
        <!--create a link to the day of the event-->
        <?php
        /*create anchor depending on which date it is*/  
        if ($event->date == '2013-08-09'){
          echo '<p class="date">'. anchor('friday', 'go to day of event').'</p>';
        } else if ($event->date == '2013-08-10'){
          echo '<p class="date">'. anchor('saturday', 'go to day of event').'</p>';
        } else if($event->date == '2013-08-11'){
          echo '<p class="date">'. anchor('sunday', 'go to day of event').'</p>';
        }
        ;?>

        <hr>
      </article>                      
    </div><!--end of 1st row-->

  <?php endforeach; endif; ?>

</div>
</div>