<div class = "row-fluid">
<div class = "span9 offset1">
<div id="top"></div>
<!--if no id then print "create a new image" otherwise print "edit image' followed by its title-->
<h3><?php echo empty($event->id) ? 'Add a new image' : 'Edit Image ' . $event->title; ?></h3>

<?php if (!isset($event->id))  {
$event->id = NULL;
}; ?>
<!--FORM-->
    <?php echo form_open_multipart('admin/event/do_upload/' . $event->id);?> <td>

    <p class="currentImage">Current image</p>
    <?php if(isset($event->img_source)): ?>
    <?php $img = $event->img_source; ?>
	<div class="row-fluid">
	<div class="span4 offset"><?php echo img('uploads/'.$img); ?></div>
	</div>
    <?php endif; ?>
	<br /><br />

    <?php echo form_upload('userfile', set_value('img_source', $event->img_source)); ?>
    <br /><br />
	<input type="submit" value="upload" />
	<?php echo form_close();?>

<!--flashdata for response to upload-->
	<div id="infoMessage"><?php echo $this->session->flashdata('error');?></div>
	<div id="successMessage">
		 <?php if($this->session->flashdata('message')){ ;?>	

		 	<?php $data = $this->session->flashdata('message'); ?>
		 	<pre><?php //print_r($data); ?></pre>						
			<ul class ="imageSuccess">
			<h5>Welldone your image uploaded </h5>
			<li>Your image data: </li>
			<?php foreach ($data{'upload_data'} as $item => $value):?>
			<li><?php echo $item;?>: <?php echo $value;?></li>
			<?php endforeach; ?>
			</ul>
	
		 <?php } ;?>

	</div>
		
 <p><?php echo anchor('admin/event','back to events', 'class="btn btn-primary btn-custom"'); ?></p>

</div>
</div>