<?php $this->load->view('admin/components/page_head_modal.php'); ?>

<body style="background: black;"><!--override the css value of body-->
	<div class="modal show" role="dialog">

		<?php $this->load->view($subview); // Subview is set in controller ?>

		<div class="modal-footer">
			&copy; <?php echo date('M, Y'); ?> <?php echo $meta_title; ?>
		</div>
	</div>

	<?php $this->load->view('admin/components/page_tail'); ?>