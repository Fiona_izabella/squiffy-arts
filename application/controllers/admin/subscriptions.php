<?php
class Subscriptions extends Admin_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('subscription_m');
	}

	public function index ()
	{
		// Fetch all subscriptions
		$this->data['subscriptions'] = $this->subscription_m->get();
		
		// Load view
		$this->data['subview'] = 'admin/subscriptions/index';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function edit ($id = NULL)
    {
        
        //need this for the set value() in the view
        if ($id) {
            $this->data['subscriptions'] = $this->subscription_m->getby($id);
           
        }
        else {
            $this->data['subscriptions'] = $this->subscription_m->get_new();//get_new creates a new object//returns (object) $subscription
            //then can use like $subscriptions->name  ... if was an array would fo $sucscriptions['name'] instead.... 
        }
        
        // first set up the rules made in model
        $rules = $this->subscription_m->rules;
        $this->form_validation->set_rules($rules);
        
          // Process the form
        //if passes validation
         if ($this->form_validation->run() == TRUE) {
         /*  $data = $this->input->post();*/
           /*$data[$field] = $this->input->post($field);*/
           $now = date('Y-m-d H:i:s');
           
           $data = array(
               'name' => $this->input->post('name'),
               'email' => $this->input->post('email'),
               'created' => $now
            );


            //$this->support_m->save($data);//create the function in the model and pass the data or..
            $this->db->insert('subscriptions', $data);
            redirect("admin/subscriptions");

          }
		
		// Load the view
		$this->data['subview'] = 'admin/subscriptions/edit';
		$this->load->view('admin/_layout_main', $this->data);	

  }
  

	public function delete ($id)
	{
		$this->subscription_m->delete($id);
		redirect('admin/subscriptions');
	}

}