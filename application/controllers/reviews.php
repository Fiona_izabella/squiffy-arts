<?php

class Reviews extends Frontend_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('support_m');
    }

    public function index() {


       // Count all reviews     
        $count = $this->db->count_all_results('reviews');
        
        // Set up pagination
        $perpage = 4;
        if ($count > $perpage) {
            $this->load->library('pagination');
            $config['base_url'] = site_url($this->uri->segment(1) . '/');
            $config['total_rows'] = $count;
            $config['per_page'] = $perpage;
            $config['uri_segment'] = 2;
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(2);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        
        
       // Fetch all reviews and store in $reviews array
       
        $this->db->order_by('create', 'desc');
        $this->db->limit($perpage, $offset);
        $this->data['reviews'] = $this->support_m->get();
        $this->data['title'] = $this->uri->segment(1);
        
        // Load the view
        $this->data['reviewcount'] = $count;
        $this->data['subview'] = 'reviews';
        $this->load->view('_main_layout', $this->data);
    }


    public function addReview ($id = NULL)
    {

         // Count all reviews     
        $count = $this->db->count_all_results('reviews');
        // Set up pagination
        $perpage = 4;
        if ($count > $perpage) {
            $this->load->library('pagination');
            $config['base_url'] = site_url($this->uri->segment(1) . '/');
            $config['total_rows'] = $count;
            $config['per_page'] = $perpage;
            $config['uri_segment'] = 2;
            $this->pagination->initialize($config);
            $this->data['pagination'] = $this->pagination->create_links();
            $offset = $this->uri->segment(2);
        }
        else {
            $this->data['pagination'] = '';
            $offset = 0;
        }
        
        // Fetch a review or set a new one
        //need this for the set value() in the view
        if ($id) {
            $this->data['reviews'] = $this->support_m->get($id);
            count($this->data['reviews']) || $this->data['errors'][] = 'article could not be found';
        }
        else {
            $this->data['reviews'] = $this->support_m->get_new();
        }
        
        // Set up the form
        // firstset up the rules made in support_m
        $rules = $this->support_m->rules;
        $this->form_validation->set_rules($rules);
        
        // Process the form
        //if passes validation pass the data from the fields into the coressponding fields 
        //this function is found in MY_MODEL
        if ($this->form_validation->run() == TRUE) {
            $data = $this->support_m->array_from_post(array(
                'title', 
                'name', 
                'body', 
                'create'
            ));
            $this->support_m->save($data);
            redirect('reviews');
            
        }
        
        // Load the view by placing the add_comment template into _main_layout
        $this->db->order_by('create', 'desc');
        $this->db->limit($perpage, $offset);
        $this->data['reviewcount'] = $count;
        $this->data['reviews'] = $this->support_m->get();
        $this->data['title'] = $this->uri->segment(1);
        $this->data['subview'] = 'reviews';
        $this->load->view('_main_layout', $this->data);
    }
    

}//end of class