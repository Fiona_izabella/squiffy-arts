<!--get  $watchlistId value from url and use to make a query in the database-->
<?php $watchlistId = $this->uri->segment(3);  ?>
<?php $this->db->where('id', $watchlistId);
$q = $this->db->get('events');
$results = $q->result_array();?>

<!--loop through the $results array-->
<?php foreach ($results as $result): ?>
  <section class="row-fluid">
    <article class="span10 offset2"> 
      <h3><?php echo 'you have added "'. $result['title'] . '" to your watchlist'; ?></h3>

      <div class="row-fluid">
        <div class="span2 offset3"><h6 class=""><?php echo anchor('watchlist/show', 'watchlist'); ?></h6></div>
        <div class="span2"><h6 class="wider"><?php echo anchor('events' ,'events'); ?></h6></div>  
      </div> 

    </article>
  </section>

<?php endforeach; ?>

