<div class="row-fluid">
  <section class="span10 offset1 watchlist"> 
  <!--add background depending on whether watchlist is empty or not-->
  <?php if($bg == TRUE) : ?>
  <div class="bgWatchlist">
   <?php else : ?>
   <div class="bgWatchlistPlain">
  <?php endif ;?>
 

    <h1>watchlist</h1>
    <h3 class="wider shiftRgt"><?php echo $message ;?></h3>


    <!--create an array which only holds one of each item to avoid duplicates-->
    <?php 
    $filteredWatchlist = array_unique($watchlist);
//print_r($filteredWatchlist);
    ?>
    <!--loop throught the filtered array and use values to make a 'where' query in the database-->
    <?php foreach ($filteredWatchlist as $w): ?>
      <p><?php //echo $w ;?></p>

      <?php 
      $this->db->where('id', $w);
//here we select every column of the table
      $query = $this->db->get('events');


      foreach ($query->result_array() as $row)
      {

        $formtime = date('g:i a', strtotime($row['time']));
        /*create a div to display to page*/
        echo '<div class="row-fluid"><div class="span10 offset1">';
        $img = $row['img_source'];
        echo '<h3>'. $row['title'] .'</h3>';
        echo '<p class="date">'.date('l', strtotime($row['date'])).'</p>';
        echo '<p class="date">'. date('g:i a', strtotime($row['time'])).'</p>';  
        echo '<p class="">'.$row['body'].'</p>';
        echo '<div class="span4">'.img('uploads/'.$img).'</div>'; 
        echo anchor('watchlist/delete/' . $row['id'], 'delete', 'class="btn orange watchlist left"'); 
        echo '</div></div>';


      }

      endforeach; ?>

      </div><!--end of bgWatchlist-->
    </section>
  </div>
