<!--subnav-->
<div class="row-fluid">
  <div class="span2 offset3"><h7 class="wider"><?php echo anchor('friday', 'fri'); ?></h7></div>
  <div class="span2"><h7 class="wider"><?php echo anchor('saturday', 'sat'); ?></h7></div>
  <div class="span2"><h7 class="wider"><?php echo anchor('sunday', 'sun'); ?></h7></div>

</div> 
<!--end of subnav-->

<!--content-->                
<div class="row-fluid">
  <section class="span10 offset1"> 

    <?php 
    $counter = 0;
/*the counter increments for the hide/show 'more info' button to give each
hideshow button a different ID corresponding to the events. This is for the jquery function to work correctly.*/
if (count($events)): foreach ($events as $event): 
  $counter++;
?>

<div class="row-fluid">
  <div class="span12">

    <div class="row-fluid">

      <article class="span4 event">
        <h3><?php echo $event->img_title; ?> </h3>           
        <?php $img = get_imageinfo($event);?>
        <?php echo img(array('src'=>"uploads/$img", 'alt'=>'image for event')); ?>
      </article>

      <article class="span8">
        <div class="row-fluid">
          <div class="span10 desc">
            <!--call method get_eventblock() form CMS_helper.php to output string to page-->
            <?php echo get_eventblock($event); ?>
            <!--sliding div initial display value is set to none-->
            <div class="slidingDiv" id="slidingDiv<?php echo $counter ?>">
              <?php echo get_moreinfo($event); ?>
            </div>
          </div>

          <!--buttons-->
          <div class="span2 verticalBtns">
            <div class="btn-group btn-group-vertical">
              <div class="btn orange moreinfo showBtn" id="show<?php echo $counter ?>">more<br>info</div>
              <div class="btn orange hideBtn" id="hide<?php echo $counter ?>">close</div>


              <div class="btn orange"><?php echo anchor('watchlist/add/' . $event->id, 'watch'); ?></div>

              <!--display edit button if user is logged in. Direct to different admin depending on role(member or admin)-->
              <?php if($this->user_m->loggedin() == TRUE && $this->user_m->is_administration()== TRUE): ?>
                <div class="btn orange watchlist"><?php echo anchor('admin/event', 'edit'); ?></div>

              <?php elseif($this->user_m->loggedin() == TRUE && $this->user_m->is_administration()== FALSE):?>
                <div class="btn orange watchlist"><?php echo anchor('member/event', 'edit'); ?></div>

              <?php endif; ?>

            </div>
          </div>
        </div>
        <hr>
      </article>

    </div><!--end of 1st row-->

  </div><!--end of span 9-->

<?php endforeach; endif; ?>


</section>
</div>
<!--end of content-->