<?php
class Event extends Admin_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('event_m');
	}

	public function index ()
	{
		// Fetch all events. get() function is found in MY_Model. event_m (model) extends MY_Model.
		$this->data['events'] = $this->event_m->get();
		
		// Load view
		$this->data['subview'] = 'member/event/index';
		$this->load->view('member/_layout_main', $this->data);
	}

	public function edit ($id = NULL)
	{
		// Fetch an event or set a new one
		if ($id) {
			$this->data['event'] = $this->event_m->get($id);
			count($this->data['event']) || $this->data['errors'][] = 'event could not be found';
		}
		else {
			$this->data['event'] = $this->event_m->get_new();
		}
		
		// Set up the form
		$rules = $this->event_m->rules;
		$this->form_validation->set_rules($rules);
		
		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->event_m->array_from_post(array(
				'title', 
				'slug', 
				'body', 
				'moreinfo',
				'date',
				'time',
				'keywords',
				'img_title'
			));
			$this->event_m->save($data, $id);
			redirect('member/event');
		}
		
		// Load the view
		$this->data['subview'] = 'member/event/edit';
		$this->load->view('member/_layout_main', $this->data);
	}

	public function delete ($id)
	{
		$this->event_m->delete($id);
		redirect('member/event');
	}

	/**
	 * function for uploading images.
	 *
	 *  if upload is successful use the image_lib class to resize the uploaded image.
	 */

	function do_upload($id = NULL)
	{    


		if ($id) {
			$this->data['event'] = $this->event_m->get($id);
			count($this->data['event']) || $this->data['errors'][] = 'event could not be found';
		}
		else {
			$this->data['event'] = $this->event_m->get_new();
		}

        //$image_path = realpath(APPPATH . '../uploads');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '2000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
	
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata($error);
            $page_id= $this->uri->segment(4);
            //// Load the view
		    $this->data['subview'] = 'member/event/editImage';
		    $this->load->view('member/_layout_main', $this->data);
		}
		else
		{   

			$this->upload->do_upload();
		    $image_data = $this->upload->data();

		    $config['image_library'] = 'gd2';
			$config['source_image'] = $image_data['full_path'];//this is the source
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 300;
			$config['height'] = 250;

			$this->load->library('image_lib', $config); 

			$this->image_lib->resize();

			$this->event_m->insert_images($this->upload->data());
			$data = array('upload_data' => $this->upload->data());
			$this->session->set_flashdata('message', $data);
            $page_id= $this->uri->segment(4);
            
            //redirect to current page and show the upload image success data using flashdata session
		    redirect('member/event/do_upload/' . $page_id);

		}
	
	}


}



