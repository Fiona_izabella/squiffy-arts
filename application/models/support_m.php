<?php
class Support_m extends MY_Model
{
	/*this class is used with the Reviews controller*/
	protected $_table_name = 'reviews';
	protected $_order_by = 'create desc';
	protected $_timestamps = TRUE;
	public $rules = array(
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		), 
		'name' => array(
			'field' => 'name', 
			'label' => 'Name', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		), 
		'body' => array(
			'field' => 'body', 
			'label' => 'Body', 
			'rules' => 'trim|required|max_length[255]'
		)
	);

    //insert or update review
	public function save($data, $id = NULL){
		
		// Set timestamps
		if ($this->_timestamps == TRUE) {
			$now = date('Y-m-d H:i:s');
			$id || $data['create'] = $now;
			//$data['modified'] = $now;
		}
		
		// Insert
		if ($id === NULL) {
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();
		}
		// Update
		else {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}
		
		return $id;
	}



	public function get_new ()
	{
		$review = new stdClass();//create a new object
		$review->title = '';
		$review->name = '';
		$review->body = '';
		$review->create = date('Y-m-d');
		return $review;
	}
	

}