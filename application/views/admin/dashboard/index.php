<div class = "row-fluid">
<div class = "span9 offset1">
<h2>Recently modified events</h2>
 <table class="table table-striped">
		<thead>
			<tr>
				<th>Event</th>
				<th>Modified</th>
			</tr>
		</thead>
		<tbody>

<?php if(count($recent_events)): ?>

<?php foreach($recent_events as $event): ?>
<tr>
<td><?php echo anchor('admin/event/edit/' . $event->id, e($event->title)); ?></td>
<!--print the date of when the event was modified. use date() function to format the date-->
<td><?php echo date('Y-m-d', strtotime(e($event->modified))); ?></td>
</tr>
<?php endforeach; ?>
<?php endif; ?>
		</tbody>
	</table>

</div>
</div>