<?php
class Article extends Frontend_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('article_m');
    }

    public function index($id, $slug){
    	// Fetch the article by the order they have been created
        $this->db->order_by('created', 'desc');
		$this->data['article'] = $this->article_m->get($id);
    	
    	// Return 404 if not found
    	count($this->data['article']) || show_404(uri_string());
		
      }

        public function add ($id = NULL)
       {
        // Fetch an article or set a new one
        // need this for the set value() in the view
        if ($id) {
            $this->data['article'] = $this->article_m->get($id);
            count($this->data['article']) || $this->data['errors'][] = 'article could not be found';
        }
        else {
            $this->data['article'] = $this->article_m->get_new();
        }
        
        // Set up the form
        // firstset up the rules made in article_m
        $rules = $this->article_m->rules;
        $this->form_validation->set_rules($rules);
        
        // Process the form
        //if passes validation pass the data from the fields into the coressponding fieds 
        //this function is found in MY_MODEL
        if ($this->form_validation->run() == TRUE) {
            $data = $this->article_m->array_from_post(array(
                'title', 
                'name',
                'body' 
                
            ));
            $this->article_m->save($data, $id);
            redirect('articles');
        }
        
        // Load the view by placing the add_comment template into _main_layout variable $subview
        $this->data['subview'] = 'add_comment';
        $this->load->view('_main_layout', $this->data);
    }
}