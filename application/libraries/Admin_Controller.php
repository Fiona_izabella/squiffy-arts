<?php
class Admin_Controller extends MY_Controller
{

	function __construct ()
	{
		//this controller will extend all controllers in admin section so all contollers will have the following..
		parent::__construct();
		$this->data['meta_title'] = 'SquiFFy aDministration';
		$this->data['member_title'] = 'artisTs aDministration';
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('user_m');
		
		// Login check
		$exception_uris = array(
			'admin/user/login', 
			'admin/user/logout'
		);
		if (in_array(uri_string(), $exception_uris) == FALSE) {
			if ($this->user_m->loggedin() == FALSE) {
				redirect('admin/user/login');
			}
		}


		
	
	}
}