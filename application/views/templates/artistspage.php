<!-- Main content -->
<?php if($pagination): ?>
    <section class="pagination"><?php echo $pagination; ?></section>
<?php endif; ?>

<div class="row-fluid">
    <div class="span12">

        <div class="row-fluid artistmenu">

            <?php if(count($artists)): foreach($artists as $artist): ?>
                <div class="span2 artistLinkimg"> 
                    <!--use function artist_link() created in CMS_helper--> 
                    <?php $url = artist_link($artist); ?>
                    <h3><?php echo anchor($url, e($artist->fname)); ?></h3>
                    <?php $imgLink = e($artist->img_source1); ?>
                    <?php echo anchor($url, img(array('src'=>'uploads/thumbs/'.$imgLink))); ?>

                </div>
            <?php endforeach; ?>
        <?php else: ?>

            <td colspan="3">We could not find any artists.</td>

        <?php endif; ?> 


    </div><!---end of row-fluid artistmenu-->



</div>
</div>


