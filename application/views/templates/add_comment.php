<h2><?php echo empty($article->id) ? 'Add a new article' : 'Edit article ' . $article->title; ?></h2>

<?php echo form_open(); ?>
<table class="table comments span8 offset2" >
	<tr>
		<td>Title</td>
		<td><?php echo form_input('title', set_value('title', $article->title), 'placeholder="title", required'); ?><?php echo form_error('title', '<div class="errorArticle">', '</div>'); ?></td>
		
	</tr>
	<tr>
		<td>Name</td>
		<td><?php echo form_input('name', set_value('name', $article->name), 'placeholder="name", required'); ?><?php echo form_error('name', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>Body</td>
		<td><?php echo form_textarea('body', set_value('body', $article->body), 'placeholder="article message", required, class="ckeditor"'); ?><?php echo form_error('body', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr></tr>
	<tr>
		
		<td><?php echo form_submit('submit', 'save', 'class="btn btn-primary btn-custom"'); ?></td>
		<td><?php echo anchor('articles', 'cancel', 'class="btn btn-primary btn-custom"');?></td>
	</tr>
</table>
<?php echo form_close();?>