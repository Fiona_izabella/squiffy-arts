<div class = "row-fluid">
<div class = "span9 offset1">

<h3><?php echo empty($reviews->id) ? 'Add a new review' : 'Edit review ' . $reviews->title; ?></h3>
<?php echo form_open(); ?>
<table class="table">
	<tr>
		<td>date</td>
		<td><?php echo form_input('create', set_value('create', $reviews->create), 'class="datepicker"'); ?></td>
	</tr>
	<tr>
		<td>Title</td>
		<td><?php echo form_input('title', set_value('title', $reviews->title)); ?><?php echo form_error('title', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>Name</td>
		<td><?php echo form_input('name', set_value('name', $reviews->name)); ?><?php echo form_error('name', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	
	<tr>
		<td>Body</td>
		<td><?php echo form_textarea('body', set_value('body', $reviews->body), 'class="ckeditor"'); ?><?php echo form_error('body', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>
	</tr>
</table>
<?php echo form_close();?>

</div>
</div>