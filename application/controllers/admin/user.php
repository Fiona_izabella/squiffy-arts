<?php
class User extends Admin_Controller
{

    public function __construct ()
    {
        parent::__construct();
    }

    public function index ()
    {
        // Fetch all users and store in a variable called users which is an array
        //then we foreach the array $users to get each $user value
        //could also have just $data[], and pass just $data, but keep consistent
        $this->data['users'] = $this->user_m->get();
        
        // Load view
        $this->data['subview'] = 'admin/user/index';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function edit ($id = NULL)
    {
        // Fetch a user or set a new one
        if ($id) {
            $this->data['user'] = $this->user_m->get($id);
            //if no user can be found with that $id, add error message to the $error array
            count($this->data['user']) || $this->data['errors'][] = 'User could not be found';
        }
        else {
            //create a new user object
            $this->data['user'] = $this->user_m->get_new();
        }
        
        // Set up the form
        //get the $admin rules set up in user model
        $rules = $this->user_m->rules_admin;
        //if user has no $id (new user) then add required to the rules.
        $id || $rules['password']['rules'] .= '|required';
        $this->form_validation->set_rules($rules);
        
        // Process the form
        if ($this->form_validation->run() == TRUE) {
            //if passes validation get the values, hash the password and save to database.
            $data = $this->user_m->array_from_post(array('name', 'email', 'role', 'password'));
            $data['password'] = $this->user_m->hash($data['password']);
            $this->user_m->save($data, $id);
            redirect('admin/user');
        }
        
        // Load the view
        $this->data['subview'] = 'admin/user/edit';
        $this->load->view('admin/_layout_main', $this->data);
    }

    public function delete ($id)
    {
        $this->user_m->delete($id);
        redirect('admin/user');
    }



      public function login ()
    {
        // Redirect a user if he's already logged in
        $dashboard = 'admin/dashboard';
        $this->user_m->loggedin() == FALSE || redirect($dashboard);
        
        // Set form
        $rules = $this->user_m->rules;
        $this->form_validation->set_rules($rules);
        
       // Process form
        if ($this->form_validation->run() == TRUE) {
            $success = $this->user_m->login();//this returns a boolean value
            // We can login and redirect
            //login() returns true so continue..
            
          if($success){
                        if ($this->user_m->is_administration()== TRUE) {
                            redirect($dashboard);
                        } else {
                            redirect('member/dashboard');   
                        }
            //if login() returns false
            //set flashdata to display message
            //reload the view
        } else {
           
          
                 //$this->session->set_flashdata('error', 'That email/password combination does not exist');
                 $this->data['passwordError'] = 'That email/password combination does not exist';
                 $this->data['subview'] = 'admin/user/login';
                 $this->load->view('admin/_layout_modal', $this->data);
                 //use return to stop the code running at this point
                 return;

             } 
           
        }

        //load the modal login view
        $this->data['passwordError'] = "";
        $this->data['subview'] = 'admin/user/login';
        $this->load->view('admin/_layout_modal', $this->data);

    }//end of login



    /*log out and redirect to home page*/
    public function logout ()
    {
        $this->user_m->logout();
        redirect('welcomepage');
    }

    /*check is email is unique*/
    public function _unique_email ($str)
    {
        // Dont validate if the email already exists unless its the email of the current user       
        $id = $this->uri->segment(4);
        $this->db->where('email', $this->input->post('email'));
        !$id || $this->db->where('id !=', $id);
        $user = $this->user_m->get();
        
        if (count($user)) {
            //id you find the email in the database set errror message and return false
            $this->form_validation->set_message('_unique_email', '%s should be unique');
            return FALSE;
        }
        
        return TRUE;
    }
}