<?php
class Frontend_Controller extends MY_Controller
{

	function __construct ()
	{
		parent::__construct();
		//this controller will extend all controllers in front-end section so all contollers will have the following.
		
		// Load models, helpers and libraries
		$this->load->model('page_m');
		$this->load->model('article_m');
		$this->load->model('event_m');
		$this->load->model('artist_m');
		$this->load->model('user_m');
		$this->load->model('subscription_m');
		$this->load->helper('form');
		$this->load->library('form_validation');
	    $this->load->library('session');
		
		// Fetch navigation and titles
		$this->data['menu'] = $this->page_m->get_nested();
		$this->data['event_archive_link'] = $this->page_m->get_event_link();
		$this->data['artist_archive_link'] = $this->page_m->get_artist_link();
		$this->data['meta_title'] = config_item('site_name');

        /*for degugging*/
		if($this->user_m->loggedin() == FALSE){
         /*print_r($this->session->all_userdata()); */
		}

       
		
	}

	
	
}	