<div class = "row-fluid">
<div class = "span10 offset1">

<section>
<?php //dump($users);?>
	<h2>Users</h2>
	<?php echo anchor('admin/user/edit', '<i class="icon-plus"></i> Add a user','class="addArticle"'); ?>
	<table class="table table-striped">
		<thead>
			<tr>
			<th>Name</th>
				<th>Email</th>
				<th>Role</th>
				<th>Edit</th>
				<th>Delete</th>
				<!--learn<th><?php// echo $users[1] -> name ;?></th>this outputs the second in the array-->
			</tr>
		</thead>
		<tbody>
<?php if(count($users)): foreach($users as $user): ?>	
		<tr>
		   <td><?php echo anchor('admin/user/edit/' . $user->id, $user->name); ?></td>
			<td><?php echo anchor('admin/user/edit/' . $user->id, $user->email); ?></td>
			<td><?php echo anchor('admin/user/edit/' . $user->id, $user->role); ?></td>
			<td><?php echo btn_edit('admin/user/edit/' . $user->id); ?></td>
			<td><?php echo btn_delete('admin/user/delete/' . $user->id); ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any users.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
</section>

</div>
</div>