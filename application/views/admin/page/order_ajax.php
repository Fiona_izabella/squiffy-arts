<div class = "row-fluid">
<div class = "span9 offset1">
<?php
/*print the string we create from the get_ol() below. We pass the $pages array to the function. The $pages array
comes from the get_nested() method in page_m*/
echo get_ol($pages);

function get_ol ($array, $child = FALSE)
{
	$str = '';
	
	if (count($array)) {
		/*if no child add 'sortable' class, otherwise dont add the 'sortable' class*/
		/*the 'sortable' class is called in the javascript function at the bottom of the page*/
		$str .= $child == FALSE ? '<ol class="sortable">' : '<ol>';
		
		/*create the <li></li>*/
		foreach ($array as $item) {
			$str .= '<li id="list_' . $item['id'] .'">';
			$str .= '<div>' . $item['title'] .'</div>';
			
			// Check to see if it has any children
			if (isset($item['children']) && count($item['children'])) {
				$str .= get_ol($item['children'], TRUE);
			}
			
			$str .= '</li>' . PHP_EOL;
		}
		
		$str .= '</ol>' . PHP_EOL;
	}
    /*return the created string*/	
	return $str;
}
?>
<!--id $sortable variable is set and TRUE include the js files for the ui and nested sortable-->
<?php if(isset($sortable) && $sortable === TRUE): ?>
	   <script src="<?php echo site_url('assets/js/jquery-ui-1.9.1.custom.min.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/jquery.mjs.nestedSortable.js'); ?>"></script>
		<?php endif; ?>

<script>
$(document).ready(function(){

    $('.sortable').nestedSortable({
        handle: 'div',
        items: 'li',
        toleranceElement: '> div',
        maxLevels: 2
    });

});
</script>

</div>
</div>