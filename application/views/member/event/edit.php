<div class = "row-fluid">
	<div class = "span10 offset1">

		<div id="top"></div>
		<h3><?php echo empty($event->id) ? 'Add a new event' : 'Edit event ' . $event->title; ?></h3>
		<?php echo form_open(); ?>
		<table class="table">
			<tr>
				<td>date</td>
				<td><?php echo form_input('date', set_value('date', $event->date), 'class="datepicker"'); ?></td>
			</tr>
			<tr>
				<td>time</td>
				<td><?php echo form_input('time', set_value('time', $event->time), 'class="timepicker"'); ?></td>
			</tr>
			<tr>
				<td>Title</td>
				<td><?php echo form_input('title', set_value('title', $event->title), 'title="title should be the same as slug"'); ?><?php echo form_error('title', '<div class="errorArticle">', '</div>'); ?></td>
			</tr>
			<tr>
				<td>Slug</td>
				<td><?php echo form_input('slug', set_value('slug', $event->slug), 'title="slug should be the same as title"'); ?><?php echo form_error('slug', '<div class="errorArticle">', '</div>'); ?></td>
			</tr>
			<tr>
				<td>Body</td>
				<td><?php echo form_textarea('body', set_value('body', $event->body), 'class="ckeditor" , id="textarea_id" '); ?><?php echo form_error('body', '<div class="errorArticle">', '</div>'); ?></td>
			</tr>
			<tr>
				<td>More info</td>
				<td><?php echo form_textarea('moreinfo', set_value('moreinfo', $event->moreinfo), 'class="ckeditor"'); ?></td>
			</tr>
			<tr>
				<td>keywords</td>
				<td><?php echo form_textarea('keywords', set_value('keywords', $event->keywords), 'title="Keywords help to search for event. Type in with space between each word"'); ?><?php echo form_error('keywords', '<div class="errorArticle">', '</div>'); ?></td>


			</tr>
			<tr>
				<td>Image title</td>
				<td><?php echo form_input('img_title', set_value('img_title', $event->img_title), 'title="Add an title for your image"'); ?></td>


			</tr>
			<tr>
				<td></td>
				<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>

			</tr>


			<tr>
				<td></td>
				<td></td>
			</tr>


		</table>

		<?php echo form_close();?>

		<?php if (!isset($event->id))  {
			$event->id = NULL;
			$currentImage = "you have no image uploaded";
		} else {
			$currentImage = "your current image";
		}; ?>

		<!--If has $id then show current image-->
		<p><?php echo $currentImage;?></p>
		<?php $img = $event->img_source; ?>
		<div class="row-fluid">
			<div class="span4 offset1"><?php echo img('uploads/'.$img); ?></div>
		</div>
		<br /><br />
		<p><?php echo anchor('member/event','back to events', 'class="btn btn-primary btn-custom"'); ?></p>
		<br />
		<p><a href="<?=current_url();?>#top" class="btn btn-primary btn-custom">Go to top of page</a></p>


	</div>

</div>
</div>
<script>
	$(function(){

		var pickerOpts = {

			dateFormat:"yy-mm-dd"

		};  

		$(".datepicker").datepicker(pickerOpts);

	});
</script>