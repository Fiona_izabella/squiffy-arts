<div class = "row-fluid">
	<div class = "span9 offset1">
		<div id="top"></div>
		<h3><?php echo empty($page->id) ? 'Add a new page' : 'Edit page ' . $page->title; ?></h3>
		<?php echo form_open(); ?>
		<table class="table">
			<tr>
				<td>Parent</td>
				<!--dropdown for templates-->
				<td><?php echo form_dropdown('parent_id', $pages_no_parents, $this->input->post('parent_id') ? $this->input->post('parent_id') : $page->parent_id); ?></td>
			</tr>
			<tr>
				<td>Template</td>
				<td><?php echo form_dropdown('template', array(
					'page' => 'Page', 
					'about' => 'About', 
					'articles' => 'Articles', 
					'homepage' => 'Homepage', 
					'event' => 'Event', 
					'eventMenu' => 'Event Menu',	
					'artistspage' => 'Artist pages', 
					'welcomepage' => 'Welcome Page', 
					'forum' => 'Forum Page',
					'shop' => 'Shop Pages',
					'shopMenu' => 'Shop Menu',
					'contact' => 'Contact Pages',
					'accordion' => 'Accordion Pages'),
					$this->input->post('template') ? $this->input->post('template') : $page->template); ?></td>
				</tr>
				<tr>
					<td>Title</td>
					<td><?php echo form_input('title', set_value('title', $page->title), 'title="Title should be the same as slug"'); ?><?php echo form_error('title', '<div class="errorArticle">', '</div>'); ?></td>
				</tr>
				<tr>
					<td>Add to main menu</td>
					<!--use current value of main-menu to keep check box ticked or not->-->
					<?php if (!isset($page->id))  {
                     $page->main_menu = NULL;} ?>
					<?php if($page->main_menu == 0): ?>
					<td><?php echo form_checkbox('main_menu', '1', FALSE, 'title="Select for page to be in main navigation"'); ?></td>
				    <?php else: ?>
				    <td><?php echo form_checkbox('main_menu', '1', TRUE, 'title="Select for page to be in main navigation"'); ?></td>
				     <?php endif; ?>
				</tr>
				<tr>
					<td>Slug</td>
					<td><?php echo form_input('slug', set_value('slug', $page->slug), 'title="Slug should be the same as title"'); ?><?php echo form_error('slug', '<div class="errorArticle">', '</div>'); ?></td>
				</tr>
				<tr>
					<td>Body</td>
					<td><?php echo form_textarea('body', set_value('body', $page->body), 'class="ckeditor"'); ?><?php echo form_error('body', '<div class="errorArticle">', '</div>'); ?></td>
				</tr>
				<tr>
					<td>More content</td>
					<td><?php echo form_textarea('morecontent', set_value('morecontent', $page->morecontent), 'class="ckeditor"'); ?></td>
				</tr>
				<tr>
					<td></td>
					<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>
				</tr>
			</table>
			<?php echo form_close();?>

			<!--for to upload images-->
			<?php if (!isset($page->id))  {
			$page->id = NULL;
		    }
			echo form_open_multipart('admin/page/do_upload/' . $page->id);?>
			<!--need to set $id to null if we are adding a new artist with no previous $id-->
			<?php 
			if (!isset($page->img_source)){
			$currentImage = "You have no image uploaded";
			} else {
			$currentImage = "Your current image";
			}; ?>
			<p><?php echo  $currentImage;?></p>
			<?php $img = $page->img_source; ?>
			<div class="row-fluid">
				<div class="span4 offset1"><?php echo img('uploads/page_uploads_thumbs/'.$img); ?></div>
			</div>
			<br /><br />
			<?php echo form_upload('userfile', set_value('img_source', $page->img_source)); ?>
			<br /><br />

			<input type="submit" value="upload" />
			<?php echo form_close();?>
			<!--flashdata for response to upload-->
			<div id="infoMessage"><?php echo $this->session->flashdata('error');?></div>
			<div id="successMessage">
				<?php if($this->session->flashdata('message')){ ;?>	

				<?php $data = $this->session->flashdata('message'); ?>					
				<ul class ="imageSuccess">
					<h5>Welldone your image uploaded </h5>
					<li>Your image data: </li>
					<?php foreach ($data{'upload_data'} as $item => $value):?>
						<li><?php echo $item;?>: <?php echo $value;?></li>
					<?php endforeach; ?>
				</ul>

				<?php } ;?>

			</div>
			<p><a href="<?=current_url();?>#top" class="btn btn-primary btn-custom">Go to top of page</a></p>
			<br/><br/>
			<p><?php echo anchor('admin/page','back to pages', 'class="btn btn-primary btn-custom"'); ?></p>


		</div>
	</div>
