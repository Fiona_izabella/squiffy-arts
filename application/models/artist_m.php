<?php
class Artist_m extends MY_Model
{
	protected $_table_name = 'artists';
	protected $_order_by = 'id desc';
	protected $_timestamps = TRUE;
	public $rules = array(
		'fname' => array(
			'field' => 'fname', 
			'label' => 'first name', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		), 
		'lname' => array(
			'field' => 'lname', 
			'label' => 'lasr name', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		), 
		'slug' => array(
			'field' => 'slug', 
			'label' => 'Slug', 
			'rules' => 'trim|required|max_length[100]|url_title|xss_clean'
		), 
		'email' => array(
			'field' => 'email', 
			'label' => 'Email', 
			'rules' => 'trim|required|valid_email|callback__unique_email|xss_clean'
		),
		'profile' => array(
			'field' => 'profile', 
			'label' => 'profile', 
			'rules' => 'trim|required|max_length[1200]'
		),
		'facebook' => array(
			'field' => 'facebook', 
			'label' => 'facebook', 
			'rules' => 'required|trim|max_length[256]|xss_clean|prep_url|valid_url_format|url_exists|callback_duplicate_URL_check'
		), 
		'twitter' => array(
			'field' => 'twitter', 
			'label' => 'twitter', 
			'rules' => 'required|trim|max_length[256]|xss_clean|prep_url|valid_url_format|url_exists|callback_duplicate_URL_check'
		), 
		'blog' => array(
			'field' => 'blog', 
			'label' => 'blog', 
			'rules' => 'required|trim|max_length[256]|xss_clean|prep_url|valid_url_format|url_exists|callback_duplicate_URL_check'
		), 
		'website' => array(
			'field' => 'website', 
			'label' => 'website', 
			'rules' => 'required|trim|max_length[256]|xss_clean|prep_url|valid_url_format|url_exists|callback_duplicate_URL_check'
		)
		
	);

      //create a new empty object//Create your generic data object.
	public function get_new ()
	{
		$artist = new stdClass();
		$artist->fname = '';
		$artist->lname = '';
		$artist->slug = '';
		$artist->email = '';
		$artist->profile = '';
		$artist->created = date('Y-m-d');
		$artist->img_source1 = '';
		$artist->img_source2 = '';
		$artist->img_source3 = '';
		$artist->facebook = '';
		$artist->twitter = '';
		$artist->blog = '';
		$artist->website = '';
		return $artist;
	}
	
/** [method for insetinf image into database. pass fieldname and $image_data array into method] */
	function insert_image($fieldname, $image_data = array())
	//place $field name variable as parameter into insert_image method
	//use an if statement to determine the $fieldname value
	{
	      if($fieldname == 1){
	      $data = array(
	          'img_source1' => $image_data['file_name']
	         
                       );

	        }

		  if($fieldname == 2){
		  $data = array(
		        'img_source2' => $image_data['file_name']
		          
                 		);

		   }

         if($fieldname == 3){
        $data = array(
          'img_source3' => $image_data['file_name']
         
                     );

           }
      
	        //update database where id = the value of segment(5)
	       $this ->db->where('id', $this->uri->segment(5));
	       $this->db->update('artists', $data);
	}

   /*function to insert new artist if (no id) or update artist (if has id)*/
	public function saveArtist($data, $id = NULL){
		
		// Set timestamps
		if ($this->_timestamps == TRUE) {
			$now = date('Y-m-d H:i:s');
			$id || $data['created'] = $now;
		}
		
		// Insert if $id is null
		if ($id === NULL) {
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();
		}
		// Update
		else {
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}
		
		return $id;
	}



}