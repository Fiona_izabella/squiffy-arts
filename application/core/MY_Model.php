<?php
class MY_Model extends CI_Model {

	//we extend all models from this model so we have the generic functionality eveywhere

	//set up the basic variables with default values
	protected $_table_name = '';
	protected $_primary_key = 'id';
	protected $_primary_filter = 'intval';//intval casts any input as an integar
	protected $_order_by = '';
	public $rules = array();
	protected $_timestamps = FALSE;
	
	function __construct() {
		parent::__construct();
	}
	
	/** [get value from $fields array using $_POST and store in $data array] */
	public function array_from_post($fields){
		$data = array();
		foreach ($fields as $field) {
			$data[$field] = $this->input->post($field);
		}
		return $data;
	}
	
	/** 
	 * method to get all results from database
	 *
	 * if has $id returns a row, if $single is FALSE return all records
	 * returns all records if no id
	 */
	
	public function get($id = NULL, $single = FALSE){
		
		if ($id != NULL) {
			//if we have an id then we need a single record (row)
			$filter = $this->_primary_filter;
			$id = $filter($id);//added security by filering $id
			$this->db->where($this->_primary_key, $id);
			$method = 'row';
		}
		elseif($single == TRUE) {
			$method = 'row';
			
		}
		else {
			$method = 'result';
			//else we need all records
		}
		
		if (!count($this->db->ar_orderby)) {
			$this->db->order_by($this->_order_by);
		}
		return $this->db->get($this->_table_name)->$method();
		//returns result for (all), or a row for a single value
	}
	
   /** [method to get data from database using the 'where' query. 
   $where could be an array. if you only want one object returned then you have TRUE as your second parameter] */
	public function get_by($where, $single = FALSE){
		$this->db->where($where);
		//we need to pass in the where statement
		return $this->get(NULL, $single);
	}

	/** [method to save data into database. need id to be set to NULL as default] */
	public function save($data, $id = NULL){
		/** 
		 * if you pass an $id it will be an update, otherwise it will be an insert.
		 * So if you want to update an element with an id of three you do the following...
		 * $this->model->save($data, 3); if the second parameter isnt there it will insert all $data
		 */
		
		// Set timestamp with 'created' and 'modified' fields being set to timestamp value
		if ($this->_timestamps == TRUE) {
			$now = date('Y-m-d H:i:s');
			$id || $data['created'] = $now;
			$data['modified'] = $now;
		}
		
		// Insert
		if ($id === NULL) {
			!isset($data[$this->_primary_key]) || $data[$this->_primary_key] = NULL;
			$this->db->set($data);
			$this->db->insert($this->_table_name);
			$id = $this->db->insert_id();//set the $id value using last insert id php function
		}
		// Update
		else {
			$filter = $this->_primary_filter;//filter the primary key
			$id = $filter($id);
			$this->db->set($data);
			$this->db->where($this->_primary_key, $id);
			$this->db->update($this->_table_name);
		}
		
		return $id;
	}
	
	/** [function to delete item from database by its $id. add limit to only delete one item at a time] */
	public function delete($id){
		$filter = $this->_primary_filter;
		$id = $filter($id);
		
		if (!$id) {
			return FALSE;
		}
		$this->db->where($this->_primary_key, $id);
		$this->db->limit(1);
		$this->db->delete($this->_table_name);
	}
}