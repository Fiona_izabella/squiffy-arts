<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="keywords" content="squiffy arts festival " />
    <meta name="description" content="arts community festival in London" />
    <meta name="author" content="fiona przybylski" />
    <meta name="copyright" content="&copy;fiography" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
	<title><?php echo $meta_title; ?></title>
	<!-- Bootstrap -->
	<link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('assets/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('assets/css/admin.css'); ?>" rel="stylesheet">
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/ckeditor/ckeditor.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo site_url('assets/js/ckfinder/ckfinder.js'); ?>"></script>
</head>
<body>

