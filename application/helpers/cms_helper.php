<?php
/*function to help with adding a title*/
function add_meta_title ($string)
{
    $CI =& get_instance();
    $CI->data['meta_title'] = e($string) . ' - ' . $CI->data['meta_title'];
}
/*function to help with adding a bootstrap icon*/
function btn_edit ($uri)
{
    return anchor($uri, '<i class="icon-edit"></i>');
}
/*function to help with adding a bootstrap icon. Adds javascript to aid in usability*/
function btn_delete ($uri)
{
    return anchor($uri, '<i class="icon-remove"></i>', array(
        'onclick' => "return confirm('You are about to delete a record. This cannot be undone. Are you sure?');"
        //this is javascript to add message about deleteing
    ));
}
/*function to help with adding link to article*/
function article_link($article){
    return 'article/' . intval($article->id) . '/' . e($article->slug);
}
/*function to help with adding link to event*/
function event_link($event){
    return 'event/' . intval($event->id) . '/' . e($event->slug);
}
/*function to help with adding link to artist
*eg of use: <?php $url = artist_link($artist); ?>
*then use the value of $url in anchor.
**/
function artist_link($artist){
    return 'artist/' . intval($artist->id) . '/' . e($artist->slug);
}

/*function to help with adding link to artists facebook*/
function artist_outsidelink($artist){
    return e($artist->facebook);
}

/*function to help with creating output string in articles view*/
function article_links($articles){
    $string = '<ul>';
    foreach ($articles as $article) {
        $url = article_link($article);
        $string .= '<li>';
        $string .= '<h3>' . anchor($url, e($article->title)) .  ' ›</h3>';
        $string .= '<p class="pubdate">' . e($article->pubdate) . '</p>';
        $string .= '</li>';
    }
    $string .= '</ul>';
    return $string;
}

/*function to help with creating an output string in the form of list in event view*/
function event_links($events){
    $string = '<ul>';
    foreach ($events as $event) {
        $url = article_link($event);
        $string .= '<li>';
        $string .= '<h3>' . anchor($url, e($event->title)) .  ' ›</h3>';
        $string .= '<p class="date">' . e($event->pubdate) . '</p>';
        $string .= '</li>';
    }
    $string .= '</ul>';
    return $string;
}
/*function to help with creating an output string in articles section*/
function get_excerpt($article, $numwords = 655){
    $timestamp = strtotime($article->created);
    $string = '';
    $string .= '<h4>Title: ' . e($article->title) .  '</h4>';
    $string .= '<h3>Author: ' . e($article->name) .  '</h3>';
    $string .= '<p class="date">Published: ' . date('d/m/Y', $timestamp) . ' at '. date('g:i a', $timestamp) .'</p>';
    $string .= '<p>' . e(limit_to_numwords(strip_tags($article->body), $numwords)) . '</p>';
    return $string;
}

/*function to help with creating an output string in artists section*/
function get_artist($artist, $numwords = 50){
    $string = '';
    $url = artist_link($artist);
    $string .= '<h2>' . anchor($url, e($artist->fname)) .  '</h2>';
    $string .= '<p class="pubdate">' . e($artist->lname) . '</p>';
     $string .= '<p class="email">' . e($artist->email) . '</p>';
    $string .= '<p>' . e(limit_to_numwords(strip_tags($artist->profile), $numwords)) . '</p>';
   $string .= '<p>' . anchor($url, 'Read more ›', array('fname' => e($artist->fname))) . '</p>';
    return $string;
}

/*function to help with creating an output string in events section*/
function get_eventinfo($event, $numwords = 50){
    $string = '';
    $url = event_link($event);
    $string .= '<h2>' . anchor($url, e($event->title)) .  '</h2>';
    $string .= '<p class="date">' . e($event->date) . '</p>';
    $string .= '<p>' . e(limit_to_numwords(strip_tags($event->body), $numwords)) . '</p>';
     $string .= '<p>' . e(strip_tags($event->moreinfo)) . '</p>';
    return $string;
}

/*function to help output image information*/
function get_imageinfo($event){
    $string = '';
    $string = e($event->img_source);
    return $string;
}

 /**
 *function to help with creating an output string in events section
 *example of use: <?php echo get_eventblock($event); ?>
 */
function get_eventblock($event, $numwords = 200){
    $eventtime = e($event->time);
    $day = e($event->date);
    $formtime = date('g:i a', strtotime($event->time));
    $formdate = date('l', strtotime($event->date));
    $string = '';
    $url = event_link($event);
    $string .= '<h2>'. e($event->title) .  '</h2>';
    $string .= '<h2 class="date"> date: ' . $formdate . '</h2>';
    $string .= '<h2 class="date"> time: ' . $formtime . '</h2>';
    $string .= '<p>' . e(limit_to_numwords(strip_tags($event->body), $numwords)) . '</p>';
    return $string;
}
/*function to help output string for getting more_info data*/
function get_moreinfo($event){
     $string = '';
     $string .= '<p>' . e(strip_tags($event->moreinfo)) . '</p>';
     return $string;

}

/*function to help limiting words in $excerpt*/
function limit_to_numwords($string, $numwords){
    $excerpt = explode(' ', $string, $numwords + 1);
    if (count($excerpt) >= $numwords) {
        array_pop($excerpt);
    }
    $excerpt = implode(' ', $excerpt);
    return $excerpt;
}

//create a function called e which returns a safer string
function e($string){
    return htmlentities($string);
}

/** 
 *function to create the main menu
 *
 * Creates an output sting in the form of an unordered list
 * Also includes the title attribute and access keys
 */
function get_menu ($array, $child = FALSE)
{

    $CI =& get_instance();
    $str = '';
    $n = 0;
    
    if (count($array)) {
        $str .= $child == FALSE ? '<ul class="nav">' . PHP_EOL : '<ul class="dropdown-menu">' . PHP_EOL;
        

        foreach ($array as $item) {
          
      //check if checkbox main_menu value ==1..ie true
     // if $item['mainmenu']==true ...go ahead

      if ($item['main_menu']== "1"){
        //increment variable for access keys
        $n++;
    
        $active = $CI->uri->segment(1) == $item['slug'] ? TRUE : FALSE;
        $str .= $active ? '<li class="active">' : '<li>';
        $str .= '<a href="' . site_url($item['slug']) . '" title="'.e($item['title']).' | Accesskey:'.$n.'" accesskey="'.$n.'">' . e($item['title']) . '</a>';
        $str .= '</li>' . PHP_EOL;
            }
        }
        
        $str .= '</ul>' . PHP_EOL;
    }
    
    return $str;

}

/**
 * Dump helper. Functions to dump variables to the screen in a nicley formatted manner.
 * @author Joost van Veen
 * @version 1.0
 */
if (!function_exists('dump')) {
    function dump ($var, $label = 'Dump', $echo = TRUE)
    {
        // Store dump in variable 
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        
        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';
        
        // Output
        if ($echo == TRUE) {
            echo $output;
        }
        else {
            return $output;
        }
    }
}


if (!function_exists('dump_exit')) {
    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
        dump ($var, $label, $echo);
        exit;
    }
}