<?php
class MY_Session extends CI_Session
{
//override the method sess_update
//only sess_update if it is not an ajax call as it interferes with ajax requests.
	function sess_update ()
	{
		// Listen to HTTP_X_REQUESTED_WITH
		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] !== 'XMLHttpRequest') {
			// This is NOT an ajax call
			parent::sess_update();
		}
	}
}