<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="keywords" content="squiffy arts festival london" />
  <meta name="description" content="arts community festival in London" />
  <meta name="author" content="fiona przybylski" />
  <meta name="copyright" content="&copy;fiography" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <!--get title which is passed down from MY_Controller-->
  <title><?php echo $meta_title; ?></title>
  <!-- Bootstrap and jquery css -->
  <link href="<?php echo site_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo site_url('assets/css/jquery-ui-1.10.3.custom.css'); ?>" rel="stylesheet">
  <!-- my styles depending on the cookie value-->
  <?php if(isset($_COOKIE['pagecontrast']) && ($_COOKIE['pagecontrast'] == 'contrast')) :?>
    <link href="<?php echo site_url('assets/css/contrast.css'); ?>" rel="stylesheet">
  <?php  else: ?>
    <link id="stylesheet" href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
  <?php endif; ?>


</head>
<body>
  <div class="container">
    <div class="row-fluid">
      <div class="span12">

        <!--header-->
        <header class="row-fluid">
          <div class="span12"><!--header row for nav-->

            <!--topSectionpart1-->
            <div class="row-fluid">  
              <div class="span12">
                <div class="navbar">
                  <div class="navbar-inner span12">
                    <ul class="nav top">
                      <!--link determined by whether user is logged in AND the role of the admin-->
                      <?php if($this->user_m->loggedin() == FALSE): ?>
                        <li class="span3 offset1"><?php echo anchor('admin/dashboard','log in'); ?></li>  

                      <?php elseif($this->user_m->loggedin() == TRUE && $this->user_m->is_administration()== TRUE): ?>
                        <li class="span2"><?php echo anchor('admin/user/logout', 'logout'); ?></li>  
                        <li class="span2"><?php echo anchor('admin/dashboard', 'admin'); ?></li> 

                      <?php elseif($this->user_m->loggedin() == TRUE && $this->user_m->is_administration()== FALSE): ?>
                        <li class="span2"><?php echo anchor('admin/user/logout', 'logout'); ?></li>  
                        <li class="span2"><?php echo anchor('member/dashboard', 'admin'); ?></li> 

                      <?php endif; ?>  
                      <li class="span2"><?php echo anchor('watchlist/show','watchlist'); ?></li>                         

                      <li class="icon span1 offset2 top"><?php echo anchor_popup('https://www.facebook.com/fiona.sibilski', img(array('src'=>'assets/images/frontpage/twitter.png', 'alt'=>'twitter icon'))); ?></li>
                      <li class="icon span1 top"><?php echo anchor_popup('https://www.facebook.com/fiona.sibilski', img(array('src'=>'assets/images/frontpage/facebook.png', 'alt'=>'facebook icon'))); ?></li> 
                       <li class="icon span1 top"><?php echo anchor_popup('feed', img(array('src'=>'assets/images/frontpage/rss.png', 'alt'=>'rss icon'))); ?></li> 
                    </ul>
                  </div><!--end of nav-inner-->
                </div><!--end of navbar pull-right-->
              </div>
            </div>
            <!--endOftopSectionpart1-->  

            <div id="mainSearch" class="ui-widget">      
              <?php echo form_open('search'); ?>
              <input id="search" name="search" type="text" placeholder="Search..." required />
              <?php echo form_submit('searchbutton', 'Search', 'class="btn-custom searchBtn"'); ?> 
              <?php echo form_close(); ?>              
            </div>

            <!--logo-->
            <div class="span12 logoHolder">
              <h1 class="logo">squiffy arTs</h1>
            </div>
            <!--end of logo-->
