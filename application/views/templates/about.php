  <div class="row-fluid">
         <div class="span2 offset3"><h6 class="wider5"><?php echo anchor('about','about'); ?></h6></div>
        <div class="span2"><h6 class="wider5"><?php echo anchor('pressrelease','press'); ?></h6></div>
        <div class="span2"><h6 class="wider2"><?php echo anchor('reviews','reviews'); ?></h6></div>
    
  </div> 
  <!--end of subnav-->

<!--content-->
<div class="row-fluid">
   <section class="span12 about">
      <div class="row-fluid">
                <article class="span6">
                <h2><?php echo e($page->title); ?></h2>
                 <?php $img = $page->img_source;?>
                   <?php echo img("uploads/page_uploads_thumbs/$img"); ?>
                 <?php echo $page->body; ?> 
                </article>

                <article class="span6">                        
                  <?php echo $page->morecontent; ?> 
                </article>
          </div> 
      </section>
  </div>
<!--end of content-->