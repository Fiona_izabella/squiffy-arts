<div class = "row-fluid">
<div class = "span10 offset1">

<h3><?php echo 'Edit subscription of ' . $subscriptions->email; ?></h3>
<?php echo form_open(); ?>
<table class="table">
	<tr>
		<td>Date created</td>
		<td class="bolder"><?php echo date('d/m/y g:i:a', strtotime($subscriptions->created)); ?></td>
	</tr>
	<tr>
		<td>Name</td>
		<td><?php echo form_input('name', set_value('name', $subscriptions->name)); ?><?php echo form_error('name', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>Email</td>
		<td><?php echo form_input('email', set_value('email', $subscriptions->email)); ?><?php echo form_error('email', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	
	
		<td></td>
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>
	</tr>
	
</table>
<?php echo form_close();?>


</div>
</div>