<div class = "row-fluid">
<div class = "span9 offset1">
<!--if no id then print "create a new article" otherwise print "edit article' followed by its title-->
<h3><?php echo empty($article->id) ? 'Add a new article' : 'Edit article ' . $article->title; ?></h3>
<?php echo form_open(); ?>
<!--create table which includes the original values and set up a separate error message for each form_input-->
<table class="table">
	<tr>
		<td>Title</td>
		<td><?php echo form_input('title', set_value('title', $article->title)); ?><?php echo form_error('title', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>Name</td>
		<td><?php echo form_input('name', set_value('name', $article->name)); ?><?php echo form_error('name', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	
	<tr>
		<td>Body</td>
		<td><?php echo form_textarea('body', set_value('body', $article->body), 'class="ckeditor"'); ?></td>
		<td><?php echo form_error('body', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>
	</tr>
</table>
<?php echo form_close();?>

</div>
</div>