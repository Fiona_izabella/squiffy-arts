<div class="modal-header">
<div class="row-fluid">
<div class="span1 pull-right"><?php echo anchor('welcomepage', '<i class="icon-remove"></i>'); ?></div>
</div>
	<h2>Log in</h2>
	<p>Please log in with your email and password details</p>
</div>
<div class="modal-body">
<!--print out flashdata error message if login fails-->
	<p class = "emailflashdata">
		<?php if ($this->session->flashdata('error') != ''): 
        echo $this->session->flashdata('error'); 
        endif; ?>
    </p>
    
<?php echo form_open();?>
<table class="table login">
<p class = "emailflashdata"><?php echo $passwordError;?></p>
	<tr>
		<td>Email</td>
		<td><?php echo form_input('email'); ?><?php echo form_error('email', '<div class="logErrors">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>Password</td>
		<td><?php echo form_password('password'); ?><?php echo form_error('password', '<div class="logErrors">', '</div>'); ?></td>
	</tr>
	<tr>
		<td></td>
		<td><?php echo form_submit('submit', 'Log in', 'class="btn-custom btn-primary btn-log"'); ?></td>
	</tr>
</table>
<?php echo form_close();?>
</div>