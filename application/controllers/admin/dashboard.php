<?php
class Dashboard extends Admin_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index() {
        //load model, get the events from the database and display by the order in which they have been modified
        $this->load->model('event_m');
        $this->db->order_by('modified asc');
        $this->data['recent_events'] = $this->event_m->get();      
        $this->data['subview'] = 'admin/dashboard/index';
        $this->load->view('admin/_layout_main', $this->data);
    }
    
   /*load modal view for login*/ 
    public function modal() {
        $this->load->view('admin/_layout_modal', $this->data);
    }
}