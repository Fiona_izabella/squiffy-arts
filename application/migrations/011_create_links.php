<?php 

class Migration_Create_Links extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'artist_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'facebook' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'twitter' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),

			'blog' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),

			'website' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			
		));
        
        $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('links');
	}

	public function down()
	{
		$this->dbforge->drop_table('links');
	}


}
