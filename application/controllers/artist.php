<?php
class Artist extends Frontend_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('artist_m');
    }

     /*function to load each artists indivual page. Pass artists id and slug */
    public function index($id, $slug){
      
    	// Fetch the artists	
		$this->data['artistMain'] = $this->artist_m->get($id);      
    	// Return 404 if not found
    	count($this->data['artistMain']) || show_404(uri_string());
		   	
    	// Load view
        $this->data['artists'] = $this->artist_m->get();
        $this->data['artistMain'] = $this->artist_m->get($id);

        add_meta_title($this->data['artistMain']->fname);
    	$this->data['subview'] = 'artistmember';
    	$this->load->view('_main_layout', $this->data);
    }
}