<?php
class Dashboard extends Admin_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index() {
        // Fetch recently modified events. Order by their modification date.
        $this->load->model('event_m');
        $this->db->order_by('modified desc');
        $this->data['recent_events'] = $this->event_m->get();
        //load the view
        $this->data['subview'] = 'member/dashboard/index';
        $this->load->view('member/_layout_main', $this->data);
    }
    //load the modal for login interface
    public function modal() {
        $this->load->view('member/_layout_modal', $this->data);
    }
}