<!--subnav-->
    <div class="row-fluid">
        <div class="span12 additionalInfo">
            <h2 class="centre frontPage">WELCOME tO SquiFFy aRTS feSTiVal</h2>
        </div>
    </div>
<!--end of subnav-->

<!--content-->
<div class="row-fluid">
      <section class="span12">

<div id="slider" class="carousel slide">
        <div class="carousel-inner">

          <div class="item active">
            <div class='inner-item'> 
            <img src="assets/images/frontpage/cat.jpg" alt="image of a cat in moody surroundings" />  
            </div>         
           
          </div>

          <div class="item">  
               <div class='inner-item'>      
               <img src="assets/images/frontpage/bear.png" alt="image of bear on the pavement" /> 
               </div>        
           
          </div>

          <div class="item">  
               <div class='inner-item'>      
               <img src="assets/images/frontpage/pillow.png" alt="image of pillow with dramatic lighting" /> 
               </div>        
           
          </div>

          <div class="item">  
               <div class='inner-item'>      
               <img src="assets/images/frontpage/curtain.png" alt="image of a curtain shutters with light coming through" /> 
               </div>        
           
          </div>

          <div class="item">  
               <div class='inner-item'>      
               <img src="assets/images/frontpage/clown.png" alt="image of a clown hanging clothes" /> 
               </div>        
           
          </div>

           <div class="item">  
               <div class='inner-item'>      
               <img src="assets/images/frontpage/kirsty.jpg" alt="image of painting of lady in a bra" /> 
          </div>  

          </div>      
           
               


      </div><!--end of carousel-inner -->
        <!--  next and previous controls here
              href values must reference the id for this carousel -->
          <a class="carousel-control left" href="#slider" data-slide="prev">&lsaquo;</a>
          <a class="carousel-control right" href="#slider" data-slide="next">&rsaquo;</a>
      </div><!-- end of carousel slide -->
     

               

   </section>
</div>
<!--end of content-->

     
    