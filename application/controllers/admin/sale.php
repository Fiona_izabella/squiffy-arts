<?php
class Sale extends Admin_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('shop_m');
	}

	public function index ()
	{
		//fetch all sales
		$this->data['sales'] = $this->shop_m->retreive_sales();
	
		// Load subview and admin main layout
		$this->data['subview'] = 'admin/sale/index';
		$this->load->view('admin/_layout_main', $this->data);
	}


    //get data to display more info about a sale
	public function moreinfo($order_id){
		$this->data['sales'] = $this->shop_m->get_by($order_id);
		$this->data['orderdetail'] =  $this->shop_m->orderMoreinfo($order_id);

        $this->data['subview'] = 'admin/sale/moreinfo';
		$this->load->view('admin/_layout_main', $this->data);

	}

	public function deleteSale($order_id)
	{
		$this->shop_m->deleteSale($order_id);
		redirect('admin/sale');
	}

}