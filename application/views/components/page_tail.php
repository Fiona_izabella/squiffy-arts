<!--start of footer-->
    <footer class="row-fluid">
      <div class="span12">

       <div class="row-fluid">
          <ul class="span12">
              <li class="span2 offset1 topNav"><?php echo anchor('support','Support'); ?></li>
              <li class="span2 topNav"><?php echo anchor('access','Accessibility'); ?></li>
              <li class="span2 topNav"><?php echo anchor('termsandconditions','Terms'); ?></li>
              <li class="span2 topNav"><?php echo anchor('galleryscroll','Gallery'); ?></li>
              <li class="span2 topNav"><?php echo anchor('pdf','Pdf', 'title="print out PDF of all events"'); ?></li>
          </ul>
        </div>
        <div class="row-fluid">
           <div class="span12 addressDiv">
               <address>2 Kentish Town Rd london NW1 9NX  --  8 / 9 /10th August 2013 </address>
           </div>
       </div>

<!--bottom menu-->
<div class="row-fluid">
   <div class="span12 bottomNav">

    <!-- Panel Slide --><!--this only displays when subscribe link is selected-->
        <div id="toppanel">
          <div id="panelSlide">
              <div class="content clearfix">
                  <?php //echo validation_errors(); ?>
                      <!-- subscription Form -->
                      <form class="clearfix" action="page/addSubs" method="post">
                          <label for="name">name</label>
                          <input class="field" type="text" name="name" value="" placeholder="name" required/>
                          <label for="email">email</label>
                          <input class="field" type="email" name="email" placeholder="example@domain.com" autocomplete="off" id="email" value="" required/>
                          <input type="submit" name="submit" value="Subscribe " class="button" />
                          <a id="close" class="close" href="#">close</a>                 
                      </form> 
                    </div>             
              </div>
          </div>  
    <!-- end of Panel Slide -->
    
                <!-- The open/close subscription link -->
                    <div class="tab span3">
                        <a id="open" class="open" href="#">subscribe</a>
                    </div>
                <!-- The end of open/close subscription link -->
              
                 <div class="span3 contrast">
                <!--set condition for cookie to change style sheet-->
                 <?php           
                  if(isset($_COOKIE['pagecontrast']) && ($_COOKIE['pagecontrast'] == 'contrast')){ 
                    $hidden = array('pagestyle' => 'normal');
                    echo form_open('', '', $hidden);
                    echo form_submit('mysubmit', 'change style');
                    }  else {
                    $hidden = array('pagestyle' => 'contrast');
                    echo form_open('', '', $hidden);
                    echo form_submit('mysubmit', 'change style');
                    }
                    ?>
                 </div>
                <div class="span3 second"><?php echo anchor_popup('http://www.fiography.co.uk', 'website by &copy;fioGraphY'); ?></div>
                <div class="span2"><?php echo img('assets/images/sponsors/allSponsors.png');?></div>

       </div>
       </div>
<!--end of bottom menu-->

   </div><!--end of footer span 12-->
</footer>
<!--end of footer-->

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-ui-1.10.3.custom.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/global.js"></script>
<script src="<?php echo base_url(); ?>assets/js/carousel.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jQuery.ScrollTo.js"></script>
<script type="text/javascript" src="<?php echo site_url('assets/js/ckeditor/ckeditor.js'); ?>"></script>

</div>
</div>
</div>
<!--end of container-->
</body>
</html>