<?php $this->load->view('admin/components/page_head'); ?>

<div class="row-fluid">
<div class="span4"><h1><?php echo $meta_title; ?></h1></div>
<div class="span2 offset4"><h1><?php echo anchor('admin/user/logout', 'logout'); ?></h1></div>
<div class="span2"><h1><?php echo anchor('welcomepage', 'home page'); ?></h1></div>	
</div>
    <div class="navbar navbar-static-top navbar-inverse">
	    <div class="navbar-inner">
		    <ul class="nav">
		    <!--use slug value to determine the 'active' class in the admin main menu-->
		    <?php $slug = $this->uri->segment(2);?>
		    <?php $slugOrder = $this->uri->segment(3);?>

		
			    <li<?php if ($slug=="dashboard") 
		echo " class=\"active\""; ?>><a href="<?php echo site_url('admin/dashboard'); ?>">Dashboard</a></li>
			    <li<?php if ($slug=="page") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/page', 'pages'); ?></li>
			      <li<?php if ($slugOrder=="order") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/page/order', 'order_pages'); ?></li>
			     <li<?php if ($slug=="article") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/article', 'articles'); ?></li>
			      <li<?php if ($slug=="event") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/event', 'events'); ?></li>
			    <li<?php if ($slug=="user") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/user', 'users'); ?></li>
			     <li<?php if ($slug=="artist") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/artist', 'artists'); ?></li>
			     <li<?php if ($slug=="reviews") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/reviews', 'reviews'); ?></li>
			      <li<?php if ($slug=="subscriptions") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/subscriptions', 'subscriptions'); ?></li>
		 <li<?php if ($slug=="products") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/products', 'products'); ?></li>
			      <li<?php if ($slug=="rss") 
		echo " class=\"active\""; ?>><?php echo anchor('admin/rss', 'rss feed'); ?></li>
            <li<?php if ($slug=="sale") 
    echo " class=\"active\""; ?>><?php echo anchor('admin/sale', 'sales'); ?></li>
		    </ul>
	    </div>
    </div>

<!-- <div class="container"> -->
	<div class="row">
		<!-- Main content -->
	  <div class="span12">
		<?php $this->load->view($subview); ?>
	  </div>
    
    </div>

<!--jquery for ajax username taken validation-->
<script>
$(document).ready(function() {

  /* USERNAME VALIDATION */
  // use element id=name 
  $('#registration_form').find('#name').blur(function() {
        // get the value from the username field   
        var id = $('#userIdreference').html();                    
        var name = $('#name').val();
        console.log(name, id);
        // Ajax request sent to the CodeIgniter controller "ajax" method "username_taken"
        // post the username field's value
           $.post('<?php echo site_url('ajax_search/username_taken'); ?>', 
        { name : name,
        "id" : id },
        // when the Web server responds to the request
        function(result) {  
        // if the result is TRUE ie ==1 , (we need to TRIM the 1 incase of white space), write a message to the page
        if (result.trim() == '1') {
       		$('#bad_name').html("Warning: That Username is already taken.");
        } else{

	$('#bad_name').html('');

        }
      }
    );
  }); 

  /* EMAIL VALIDATION */
  // use element id=email 
  $('#registration_form').find('#email').blur(function() {
        // get the value from the username field                              
        var email = $('#email').val();
         //console.log(email)
        // Ajax request sent to the CodeIgniter controller "ajax" method "email_taken"
        // post the username field's value
          $.post('<?php echo site_url('ajax_search/email_taken'); ?>',
         { email : email },
         // when the Web server responds to the request
         function(result) {  
        // if the result is TRUE ie..1.. write a message to the page
         if (result.trim() == '1') {
       		$('#bad_email').html("Warning:That Email is already taken");
        } else{

	      $('#bad_email').html('<i class="icon-large icon-check"></i>');

        }
      }
    );
  });  

});//end of document ready
</script>


<!--load the pagetail-->
<?php $this->load->view('admin/components/page_tail'); ?>