<?php
class Page extends Admin_Controller
{

	public function __construct ()
	{
		parent::__construct();
		$this->load->model('page_m');
	}

	public function index ()
	{
		// Fetch all pages
		$this->data['pages'] = $this->page_m->get_with_parent();
		
		// Load view
		$this->data['subview'] = 'admin/page/index';
		$this->load->view('admin/_layout_main', $this->data);
	}

     /*function for sorting the pages order*/
     /*make the value of $sortable turue and load the view*/
	public function order ()
	{
		$this->data['sortable'] = TRUE;
		$this->data['subview'] = 'admin/page/order';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function order_ajax ()
	{
		// Save order from ajax call
		if (isset($_POST['sortable'])) {
			$this->page_m->save_order($_POST['sortable']);
		}
		
		// Fetch all pages
		$this->data['pages'] = $this->page_m->get_nested();
		
		// Load view
		$this->load->view('admin/page/order_ajax', $this->data);
	}

	public function edit ($id = NULL)
	{
		// Fetch a page or set a new one
		if ($id) {
			$this->data['page'] = $this->page_m->get($id);
			count($this->data['page']) || $this->data['errors'][] = 'page could not be found';
		}
		else {
			//we can call the get_new function which creates a new object so we can do $page->title and not have to do a foreach loop form an array
			$this->data['page'] = $this->page_m->get_new();
		}
		
		// Get pages for dropdown
		$this->data['pages_no_parents'] = $this->page_m->get_no_parents();
		
		// Set up the form for validation
		$rules = $this->page_m->rules;
		$this->form_validation->set_rules($rules);

		// Process the form
		if ($this->form_validation->run() == TRUE) {
			$data = $this->page_m->array_from_post(array(
				'title', 
				'slug', 
				'body', 
				'morecontent',
				'template', 
				'parent_id',
				'main_menu'
			));
			$this->page_m->save($data, $id);
			redirect('admin/page');
		}
		
		// Load the view
		$this->data['subview'] = 'admin/page/edit';
		$this->load->view('admin/_layout_main', $this->data);
	}

	public function delete ($id)
	{
		$this->page_m->delete($id);
		redirect('admin/page');
	}

	public function _unique_slug ($str)
	{
		// Do NOT validate if the slug already exists
		// Unless it's the slug for the current page
		
		$id = $this->uri->segment(4);
		$this->db->where('slug', $this->input->post('slug'));
		! $id || $this->db->where('id !=', $id);
		$page = $this->page_m->get();
		
		if (count($page)) {
			//add a new validation rule
			$this->form_validation->set_message('_unique_slug', '%s should be unique');
			return FALSE;
		}
		
		return TRUE;
	}


     /*upload image function*/
	 function do_upload()
    {    
       
        $config['upload_path'] = './uploads/page_uploads';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '1000';
        $config['max_width']  = '1024';
        $config['max_height']  = '768';
    
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());

            //display $error array onto page with flashdata session
            $this->session->set_flashdata($error);
            $page_id= $this->uri->segment(4);
            //redirect to current page getting correct slug from url segment
            redirect('admin/page/edit/' . $page_id);
          
        }
        else
        {   
            $this->page_m->insert_images($this->upload->data());
            $data = array('upload_data' => $this->upload->data());
            $image_data = $this->upload->data();
            
            // Create the Thumbnail for display
                $config['image_library'] = 'gd2';
                 $config['new_image'] = './uploads/page_uploads_thumbs/';
                $config['source_image'] = $image_data['full_path'];
                $config['maintain_ratio'] = FALSE;
                $config['width'] = 200;
                $config['height'] = 143;

                 $this->load->library('image_lib', $config);
                 $this->image_lib->resize();

                $this->page_m->insert_images($this->upload->data());
                $data = array('upload_data' => $this->upload->data());
                $this->session->set_flashdata('message', $data);
                $page_id= $this->uri->segment(4);
                redirect('admin/page/edit/' . $page_id);

               }
    
           }
}//end of class