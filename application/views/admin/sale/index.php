<div class = "row-fluid">
<div class = "span10 offset1">

	<h2>Sales</h2>
	<table class="table table-striped sales">
		<thead>
			<tr>
				<th>name</th>
				<th>surname</th>
				<th>email</th>
				<th>address</th>
				<th>phone number</th>
				<th>paid</th>
				<th>items</th>
				<th>purchased</th>
				<th>order-No.</th>
				<th>delete</th>
			</tr>
		</thead>
		<tbody>
		<!--If there are sales loop through each sale and print the following values to the page-->
<?php if(count($sales)): foreach($sales as $sale): ?>	
		<tr>
		<td><?php echo $sale->fname ;?></td>
		<td><?php echo $sale->lname ;?></td>
		<td><?php echo $sale->email ;?></td>
		<td><?php echo $sale->address ;?></td>
		<td><?php echo $sale->phonenumber ;?></td>
		<td><?php echo $sale->amount_paid ;?></td>
		<td><?php echo $sale->numbItems ;?></td>
		<td><?php echo date('Y-m-d', strtotime(e($sale->created))); ?></td>
		<td><?php echo anchor('admin/sale/moreinfo/'.$sale->order_id, $sale->order_id); ?></td>
		<td><?php echo btn_delete('admin/sale/deleteSale/' . $sale->order_id); ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any articles.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>


</div>
</div>