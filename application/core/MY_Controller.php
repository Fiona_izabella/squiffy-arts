<?php
class MY_Controller extends CI_Controller {
	
	public $data = array();//empty array
		function __construct() {
			parent::__construct();
			$this->data['errors'] = array();
			$this->data['site_name'] = config_item('site_name');
             //the profiler class helps debug. 
             //Do not leave profiler class when not using, as it interferes with ajax calls
			 //$this->output->enable_profiler(TRUE);
			 
			 /*get value from footer form and set or delete cookie*/
	 $value = $this->input->post('pagestyle');
     if( isset($value) && $value == 'contrast'){
     setcookie('pagecontrast', 'contrast', time()+3600);//expires in one hour
     redirect(current_url());

       }  else if (isset($value) && $value == 'normal'){
     setcookie('pagecontrast', 'contrast', time()-3600);//delete cookie
     redirect(current_url());

       }

			
		}
}