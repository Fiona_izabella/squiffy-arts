<div class = "row-fluid">
<div class = "span9 offset1">

<h3><?php echo empty($artist->id) ? 'Add a new artist' : 'Edit artist ' . $artist->fname; ?></h3>
<?php $attributes = array('id' => 'myform');
echo form_open("",$attributes);
//echo form_open(); ?>
 <div id="inputs">
<table class="table">
	<tr>
		<td>first name</td>
		<td><?php echo form_input('fname', set_value('fname', $artist->fname), 'id="fname" title="Name must be the same as slug."'); ?><?php echo form_error('fname', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
	<tr>
		<td>last name</td>
		<td><?php echo form_input('lname', set_value('lname', $artist->lname), 'title="Should be at least 8 characters."'); ?><?php echo form_error('lname', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>Slug</td>
		<td><?php echo form_input('slug', set_value('slug', $artist->slug), 'title="Slug must be the same as name."'); ?><?php echo form_error('slug', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>email</td>
		<td><?php echo form_input('email', set_value('email', $artist->email),'title="Must be a valid email"'); ?><?php echo form_error('email', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>	
	<tr>
		<td>profile</td>
		<td><?php echo form_textarea('profile', set_value('profile', $artist->profile), 'class="ckeditor" title="profile must not exceed 1200 characters"'); ?><?php echo form_error('profile', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>

	<tr>
		<td>facebook</td>
		<td><?php echo form_input('facebook', set_value('facebook', $artist->facebook)); ?><?php echo form_error('facebook', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>twitter</td>
		<td><?php echo form_input('twitter', set_value('twitter', $artist->twitter)); ?><?php echo form_error('twitter', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>blog</td>
		<td><?php echo form_input('blog', set_value('blog', $artist->blog)); ?><?php echo form_error('blog', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>
	<tr>
		<td>website</td>
		<td><?php echo form_input('website', set_value('website', $artist->website)); ?><?php echo form_error('website', '<div class="errorArticle">', '</div>'); ?></td>
	</tr>	
	<tr>
		<td></td>
		<td><?php echo form_submit('submit', 'Save', 'class="btn btn-primary btn-custom"'); ?></td>
	</tr>

	</div>
</table>

<?php echo form_close();?>

<div>
<h2>Your images</h2>
</div>

<?php if (!isset($artist->id))  {
$artist->id = NULL;
}; ?>

<h3>image one</h3>
<p>current image</p>
<?php $img1 = $artist->img_source1; ?>
 <p><?php echo img('uploads/thumbs/'.$img1); ?></p>
<br /><br />
<hr>

<?php if (!isset($artist->id))  {
$artist->id = NULL;
}; ?>
<h3>image two</h3>
<p>current image 2</p>
<?php $img2 = $artist->img_source2; ?>
<p><?php echo img('uploads/thumbs/'.$img2); ?></p>
<br /><br />
<hr>

<?php if (!isset($artist->id))  {
$artist->id = NULL;
}; ?>
<h3>upload image three</h3>
<p>current image 3</p>
<?php $img3 = $artist->img_source3; ?>
<p><?php echo img('uploads/thumbs/'.$img3); ?></p>
<br /><br />
<hr>


<p><?php echo anchor('member/artist','back to artists', 'class="btn btn-primary btn-custom"'); ?></p>
<br />
<p><a href="<?=current_url();?>#top" class="btn btn-primary btn-custom">Go to top of page</a></p>

</div>
</div>