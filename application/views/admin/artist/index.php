<div class = "row-fluid">
<div class = "span10 offset1">
<section>
	<h2>Artists</h2>
	<?php echo anchor('admin/artist/edit', '<i class="icon-plus"></i> Add an artist','class="addArticle"'); ?>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Artist</th>
				<th>Created</th>
				<th>Edit</th>
				<th>Edit/Add image</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
<?php if(count($artists)): foreach($artists as $artist): ?>	
		<tr>
			<td><?php echo anchor('admin/artist/edit/' . $artist->id, $artist->fname); ?></td>
			<td><?php echo $artist->created; ?></td>
			<td><?php echo btn_edit('admin/artist/edit/' . $artist->id); ?></td>
			<td><?php echo btn_edit('admin/artist/editImage/' . $artist->id); ?></td>
			<td><?php echo btn_delete('admin/artist/delete/' . $artist->id); ?></td>
		</tr>
<?php endforeach; ?>
<?php else: ?>
		<tr>
			<td colspan="3">We could not find any artists.</td>
		</tr>
<?php endif; ?>	
		</tbody>
	</table>
</section>

</div>
</div>