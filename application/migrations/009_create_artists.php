<?php 

class Migration_Create_Artists extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'fname' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
			),
			'lname' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'email' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'profile' => array(
				'type' => 'TEXT',
			),
			'img_source1' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
				'null' => TRUE,
			),
			'img_source2' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
				'null' => TRUE,
			),
			'img_source3' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
		));
        
        $this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('artists');
	}

	public function down()
	{
		$this->dbforge->drop_table('artists');
	}


}
