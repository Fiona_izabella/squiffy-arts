<?php $data = $this->session->flashdata('message'); ?>	
<?php $emailConfirm = $data['email'];?>
<?php $buyerfname = $data['fname']; ?>
	
<h3>Congratulations you  lucky <?php echo $buyerfname;?></h3>

<?php if ($cart = $this->cart->contents()): ?>
	<h1>You have spent &pound;<?php echo $this->cart->total(); ?> and ordered <?php echo $this->cart->total_items();?> items</h1>
	<h3 class="shiftRgt">but not really spent a penny ;)</h3>
	<h4 class="shiftRgt shiftDown">We have sent a receipt to <span class="bolder"><?php echo $emailConfirm  ;?></span></h4>
<?php endif; ?>
<br/>
<h2>Review your awesome goodies</h2>

<div class="row-fluid">
	<div class="span10 offset1">

		<?php if ($cart = $this->cart->contents()): ?>
			<?php //dump($cart) ;?>
			<?php foreach ($cart as $item): ?>
				<div class="row-fluid">
					<div class="span6 purchasedItem">
						<p class="date">Purchase: <?php echo $item['name']; ?></p>
						<p class="date">Quantity: <?php echo $item['qty']; ?></p>
						<img src="<?php echo base_url(); ?>assets/images/products/<?php echo $item['image']; ?>" alt="" />
					</div>  
				</div>
			<?php endforeach; ?>
		<?php endif; ?>

		<div class="row-fluid">
			<div class="span6">
				<h4><?php echo anchor('shop/emptyCart', 'return to shop') ;?></h4>
			</div>
		</div>

	</div>
</div>
