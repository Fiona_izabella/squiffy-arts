$(document).ready(function() {

	// to subscription panel
	$("#open").click(function(){
		$("div#panelSlide").slideDown("slow");

	});

	// to close subscription panel
	$("#close").click(function(){
		$("div#panelSlide").slideUp("slow");
	});


  /*tooltip function*/
$("[title]").tooltip({position: {
my: "left+120 center",
at: "centre right"

}});


/*-----jquery autocomplete function in main navigation search tool------*/
 $(function() {
    function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }

$( "#search" ).autocomplete({
      source: function( request, response ) {
            $.ajax({
              url:'http://192.168.201.215/squiffyArts/ajax_search/search_help',
              //url:'http://localhost:8888/squiffyArts/ajax_search/search_help', 
              //need the full path, cant use php inside an external javascript file
              datatype: "json",
              type: 'POST',
              data: { search : $("#search").val() },
              success: function( data ) {
              console.log(data);
              data1 = $.parseJSON(data);

              response( $.map( data1, function( item ) {
              return {
               value: item
              }
             }));
          }
        });
      },
      //minLength: 2,
      select: function( event, ui ) {
      log( ui.item ?
      "Selected: " + ui.item.label :
      "Nothing selected, input was " + this.value);
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });

 });

/*-----------bootstrap carousel function----------------*/
$('.carousel').carousel({
          interval: 100
        });
        $('.carousel').carousel('cycle');


/*function for more-info button on events page*/
/*need to make each hideshow button and slidingDiv have a different id.*/
      $(function () {
                
           $(".hideBtn").on("click", function(){
              var btnNum = $(this).attr("id");
              btnNum = btnNum.substr(4)
              
              $('#slidingDiv' + btnNum).hide();
              });

             $(".showBtn").on("click", function(){
              var btnNum = $(this).attr("id");
              btnNum = btnNum.substr(4)

              $('#slidingDiv' + btnNum).show();
          });


});
     
/*---------------parralax functions------------*/ 


  $("#s1").click(function(e){
    e.preventDefault();
    $.scrollTo("#p1", 800)
    })

  $("#s2").click(function(e){
    e.preventDefault();
    $.scrollTo("#p2", 800)
    //alert("working2")
    })

  $("#s3").click(function(e){
    e.preventDefault();
    $.scrollTo("#p3", 800)
    //alert("working3")
    })

  $("#s4").click(function(e){
    e.preventDefault();
    $.scrollTo("#p4", 800)
    //alert("working4")
    })


  //parrallax scrolling bit
  var win = $(window);
  $("div.background").each(function(){
    //alert("scrolling");

    var bg = $(this);
    win.scroll(function(){

      var yPos = -(win.scrollTop() / bg.data("speed"));
      var coords = "50% "+ yPos + "px";
      bg.css({backgroundPosition: coords});

    })

     $("div.pic").each(function(){
        var pic = $(this);
        $(window).scroll(function(){
          //this is a calcuation so that the movement only happens
          //when it is in the current view div
          if ((win.scrollTop()+ win.height()) > (bg.offset().top) && (bg.offset().top + bg.height()) > win.scrollTop()){
          var yPos = -(win.scrollTop() / pic.data("speed"));
          var coords = pic.data("xpos") + " " + (yPos + pic.data("ypos")) + "px";
          pic.css({backgroundPosition: coords});

          }

        })

     })

  })

  /*---------------end of parralax functions------------*/


	
});

	





 






