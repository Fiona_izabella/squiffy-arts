<?php
class Event_m extends MY_Model
{
	protected $_table_name = 'events';
	protected $_order_by = 'date asc, id asc';
	protected $_timestamps = TRUE;
	public $rules = array(
		'date' => array(
			'field' => 'date', 
			'label' => 'date', 
			'rules' => 'trim|required|max_length[6]|xss_clean'
		), 
		'date' => array(
			'field' => 'time', 
			'label' => 'time', 
			'rules' => 'trim|required|xss_clean'
		), 
		'title' => array(
			'field' => 'title', 
			'label' => 'Title', 
			'rules' => 'trim|required|max_length[100]|xss_clean'
		), 
		'slug' => array(
			'field' => 'slug', 
			'label' => 'Slug', 
			'rules' => 'trim|required|max_length[100]|url_title|xss_clean'
		), 
		'body' => array(
			'field' => 'body', 
			'label' => 'Body', 
			'rules' => 'trim|required'
		),
		'moreinfo' => array(
			'field' => 'moreinfo', 
			'label' => 'More info', 
			'rules' => 'trim|required'
		),
		'keywords' => array(
			'field' => 'keywords', 
			'label' => 'keywords', 
			'rules' => 'trim|required'
		),
		'img_title' => array(
			'field' => 'img_title', 
			'label' => 'Image title', 
			'rules' => 'trim|max_length[25]|xss_clean'
		)
	);

    /*create rules for the search function*/
	public $searchrules = array(
		'search' => array(
			'field' => 'search', 
			'label' => 'search', 
			'rules' => 'trim|required|max_length[25]|xss_clean'
		)
	);


   /** [create a new empty object] */
	public function get_new ()
	{
		$event = new stdClass();
		$event->title = '';
		$event->slug = '';
		$event->body = '';
		$event->moreinfo = '';
		$event->date = date('Y-m-d');
		$event->time = date('h:i:s');
		$event->created = date('Y-m-d');
		$event->keywords = '';
		$event->img_source = '';
		$event->img_title = '';
		return $event;
	}
		
	/**
	 * separate functions for each day of the event. 
	 * Query database depending on the date value of the function
	 */
	public function friday_date(){
		$this->db->where('date', '2013-08-09');
		$this->db->order_by('time', "asc");
    }

    public function sat_date(){
		$this->db->where('date', '2013-08-10');
		$this->db->order_by('time', "asc");
    }

    public function sun_date(){
		$this->db->where('date', '2013-08-11');
		$this->db->order_by('time', "asc");
    }

    /*query to display all events in chronological order in pdf printout*/
    public function pdf_events(){
		$this->db->order_by('date asc, time asc'); 
		/*do not need select(*) as if this is not stated codeigniter assumes select(*)*/

    }


/*function to search results from a search query depending on the keywords the user searches for*/
	public function keyword_search()
    {
         $keyword = $this->input->post('search');
         $this->db->like('keywords', $keyword);
         $query = $this->db->get('events');
         $query->result_array(); 
       
        foreach ($query->result() as $row){
          
            //push $row into $results array
            $results[] = $row;
        }
        
        if (mysql_affected_rows()> 0)
          { 
        return $results;

       }
    }


    public function eventResult(){
    	$keyword = $this->input->post('search');
    	return $keyword;
    }


/*function to insert and updating images into the database. Is is called by the do_upload method*/
	function insert_images($image_data = array()){
      $data = array(
          'img_source' => $image_data['file_name'],
           );
      
       $this ->db->where('id', $this->uri->segment(4));
       $this->db->update('events', $data);
 
	}



}