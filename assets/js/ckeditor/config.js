/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	/*config.menu_groups = 'googlemaps';*/
    /*config.extraPlugins = 'gmap';*/
    /*config.toolbar_YourToolbarName = [ ['Button','Button','gMap'] ];*/



	/*CKFinder.setupCKEditor( null, '../ckfinder/' );
var editor = CKEDITOR.replace( 'editor1' );*/

	/*CKEDITOR.replace( 'editor1',
{
	filebrowserBrowseUrl : '../ckfinder/ckfinder.html',
	filebrowserImageBrowseUrl : '../ckfinder/ckfinder.html?type=Images',
	filebrowserFlashBrowseUrl : '../ckfinder/ckfinder.html?type=Flash',
	filebrowserUploadUrl : '../ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Files',
	filebrowserImageUploadUrl : '../ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Images',
	filebrowserFlashUploadUrl : '../ckfinder/core/connector/asp/connector.asp?command=QuickUpload&type=Flash'
});*/

	//config.filebrowserUploadUrl = 'application/controllers/editorUploads.php';
};
